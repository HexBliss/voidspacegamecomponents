package com.universeprojects.gamecomponents.client;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.gamecomponents.client.demos.ActionProgressBarDemo;
import com.universeprojects.gamecomponents.client.demos.BlendingModeDemo;
import com.universeprojects.gamecomponents.client.demos.ColorChooserDemo;
import com.universeprojects.gamecomponents.client.demos.CompositeSpriteDemo;
import com.universeprojects.gamecomponents.client.demos.Demo;
import com.universeprojects.gamecomponents.client.demos.EnergySystemControlDemo;
import com.universeprojects.gamecomponents.client.demos.InfoDialogDemo;
import com.universeprojects.gamecomponents.client.demos.InspectorDemo;
import com.universeprojects.gamecomponents.client.demos.InventionSystemDemo;
import com.universeprojects.gamecomponents.client.demos.InventoryV2Demo;
import com.universeprojects.gamecomponents.client.demos.LabelsDemo;
import com.universeprojects.gamecomponents.client.demos.LevelGaugeDemo;
import com.universeprojects.gamecomponents.client.demos.LoginProcessDemo;
import com.universeprojects.gamecomponents.client.demos.ParticleSystemDemo;
import com.universeprojects.gamecomponents.client.demos.QuestDialogDemo;
import com.universeprojects.gamecomponents.client.demos.ScrollablePaneDemo;
import com.universeprojects.gamecomponents.client.demos.SelectionMenuDemo;
import com.universeprojects.gamecomponents.client.demos.SliderDemo;
import com.universeprojects.gamecomponents.client.demos.StackSplitterDemo;
import com.universeprojects.gamecomponents.client.demos.StoreDialogDemo;
import com.universeprojects.gamecomponents.client.demos.StoreManagerDialogDemo;
import com.universeprojects.gamecomponents.client.demos.SwitchDemo;
import com.universeprojects.gamecomponents.client.demos.TradeDialogDemo;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

import java.util.ArrayList;
import java.util.List;

class MainMenu {

    private final H5ELayer layer;
    private final GCSimpleWindow window;

    private final List<Demo> demos = new ArrayList<>();
    private final List<Demo> autoOpenDemos = new ArrayList<>();

    private int demoCounter;

    MainMenu(H5ELayer layer) {
        this.layer = Dev.checkNotNull(layer);
        // setup animation event
        layer.addAction(new Action() {
            @Override
            public boolean act(float delta) {
                for (Demo demo : demos) {
                    if (demo.isOpen()) {
                        demo.animate();
                    }
                }
                return false;
            }
        });

        window = new GCSimpleWindow(layer, "main-menu", "Main Menu");
        H5EButton btnAllDemos = addButton("Open all UI demos");
        btnAllDemos.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                for (Demo demo : demos) {
                    if (!demo.isOpen()) {
                        demo.open();
                    }
                }
            }
        });
        window.addEmptyLine(15);
        demoCounter = 0;

//        window.addLabel("Miscellaneous:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
//        addDemo(false, "Benchmarks", new BenchmarksDemo());
//        addDemo(false, "Sound", new SoundDemo(layer));

        window.addLabel("Basic building blocks:");
        addDemo("Labels", new LabelsDemo(layer));
        addDemo("Level Gauges", new LevelGaugeDemo(layer));
        addDemo("Sliders", new SliderDemo(layer));
        addDemo("Switches", new SwitchDemo(layer));
        addDemo("Color Chooser", new ColorChooserDemo(layer));
        addDemo("Action Progress Bar", new ActionProgressBarDemo(layer));
        addDemo("Scrollable Pane", new ScrollablePaneDemo(layer));
        addDemo("Composite Sprite Demo", new CompositeSpriteDemo(layer));
        addDemo("Particle System Demo", new ParticleSystemDemo(layer));

//        addDemo("Cached container", new CachedContainerDemo(layer));
//        addDemo("Composite Sprite", new CompositeSpriteDemo(layer));

        window.addEmptyLine(15);
        demoCounter = 0;

        window.addLabel("Voidspace dialog windows:");
        addDemo("Info Dialog", new InfoDialogDemo(layer));
        addDemo("Selection Menu", new SelectionMenuDemo(layer));
        addDemo("Object Inspector", new InspectorDemo(layer));
        addDemo("Inventory V2", new InventoryV2Demo(layer));
        addDemo("Stack Splitter", new StackSplitterDemo(layer));
        addDemo("Trade Dialog", new TradeDialogDemo(layer)); //not in use
//        addDemo("Group Viewer", new GroupViewerDemo(layer)); //not in use
        addDemo("Energy System Control", new EnergySystemControlDemo(layer));
        addDemo("Invention System Dialog", new InventionSystemDemo(layer));
        addDemo("Blending Modes", new BlendingModeDemo(layer));
        addDemo("Login Process Demo", new LoginProcessDemo(layer));
        addDemo("Quest Dialog", new QuestDialogDemo(layer));
        addDemo("Store Dialog", new StoreDialogDemo(layer));
        addDemo("Store Manager Dialog", new StoreManagerDialogDemo(layer));
        window.pack();
        window.positionProportionally(.2f, 0.5f);
        window.open();
    }

    private Demo addDemo(String caption, final Demo demo) {
        return addDemo(false, caption, demo);
    }

    private Demo addDemo(boolean autoOpen, String caption, final Demo demo) {
        demos.add(Dev.checkNotNull(demo));
        if (autoOpen) {
            this.autoOpenDemos.add(demo);
        }
        demoCounter++;
        if (demoCounter % 2 == 1) {
            window.row();
        }
        H5EButton btn = addButton(caption);
        btn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                demo.toggle();
            }
        });
//        Cell<H5EButton> cell = window.getWindow().getCell(btn);
//        cell.uniform();

        return demo;
    }

    private H5EButton addButton(String caption) {
        H5EButton btn = new H5EButton(caption, layer);
//        btn.getLabel().setFontScale(1);
        return window.add(btn).growX().getActor();
    }

    public void open() {
        window.open();
        for (Demo demo : autoOpenDemos) {
            demo.open();
        }
    }

}
