package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.gamecomponents.client.dialogs.GCInfoDialog;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.BlendingMode;
import com.universeprojects.html5engine.client.framework.CompositeSpriteConfiguration;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESprite;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.json.shared.JSONArray;
import com.universeprojects.json.shared.JSONObject;

import java.util.Deque;
import java.util.LinkedList;

@SuppressWarnings("SpellCheckingInspection")
public class CompositeSpriteDemo extends Demo {

    private final Logger log = Logger.getLogger(CompositeSpriteDemo.class);

    private final GCSimpleWindow controlWindow;


    private Deque<GCInfoDialog> openDialogs = new LinkedList<>();

    private int nextDialogIndex = 0;

    public CompositeSpriteDemo(H5ELayer layer) {
        controlWindow = new GCSimpleWindow(layer, "composite-sprite-demo", "Composite Sprite Demo");

        H5ESprite infectedSprite = createInfectedSprite(layer);
        controlWindow.add(infectedSprite);
//        H5ESprite infectedSprite2 = createInfectedSpriteViaVariables(layer, "Asteroid3");
//        controlWindow.add(infectedSprite2);
//        H5ESprite outlineSprite = createOutlineSprite(layer);
//        controlWindow.add(outlineSprite);
//        H5ESprite coloredBackgroundSprite = createedColorBackgroundSprite(layer);
//        controlWindow.add(coloredBackgroundSprite);
//        H5ESprite singleColorSprite = createSingleColorSprite(layer);
//        controlWindow.add(singleColorSprite);


        controlWindow.positionProportionally(.5f, .5f);
    }

    private H5ESprite createInfectedSprite(H5ELayer layer) {
        JSONObject json = new JSONObject();
        JSONArray layerDefinitions = new JSONArray();
        json.put("layerDefinitions", layerDefinitions);
        JSONObject layer1 = new JSONObject();
        layer1.put("type", "Sprite");
        layer1.put("spriteTypeKey", "infected-effect1");
        layer1.put("blendingMode", "SOURCE_IN");
        layerDefinitions.add(layer1);
        JSONObject layer2 = new JSONObject();
        layer2.put("type", "Sprite");
        layer2.put("spriteTypeKey", "shipyard1");
        layer2.put("transparency", 0.9);
        layer2.put("blendingMode", "MULTIPLY");
        layerDefinitions.add(layer2);

        H5ESpriteType primarySpriteType = (H5ESpriteType)layer.getEngine().getResourceManager().getSpriteType("shipyard1");

        H5ESprite sprite = new H5ESprite(layer, primarySpriteType);
        sprite.setBlendingMode(BlendingMode.SOURCE_OVER);
        CompositeSpriteConfiguration config = new CompositeSpriteConfiguration(json);
        sprite.setCompositeSpriteConfiguration(config);
        return sprite;
    }

//    private H5EGraphicElement createInfectedSpriteViaVariables(H5ELayer layer, String spriteKey) {
//        H5ESpriteType cst = null;
//
//        try {
//            cst = new H5ESpriteType(layer.getEngine().getResourceManager(), CompositeSpriteEffects.infected());
//            cst.addVariable("spriteKey", spriteKey);
//            cst.forceInitialize();
//        } catch (ParseException e) {
//            throw new RuntimeException(e.getMessage(), e);
//        }
//        H5ESprite compositeSprite = new H5ESprite(layer, cst);
//
//        return compositeSprite;
//    }
//
//    private H5EGraphicElement createOutlineSprite(H5ELayer layer) {
//        H5ECachedContainer cachedContainer = new H5ECachedContainer(layer);
//        H5ESpriteType imageSpriteType = layer.getEngine().getResourceManager().getSpriteType("shipyard1");
//        H5ESprite bottomLayer = new H5ESprite(layer, imageSpriteType);
//        bottomLayer.setShadowColor("red");
//        bottomLayer.setShadowBlur(3);
//        H5ESprite topLayer = new H5ESprite(layer, imageSpriteType);
//        topLayer.setBlendingMode(BlendingMode.XOR);
//        cachedContainer.addChild(bottomLayer);
//        cachedContainer.addChild(topLayer);
//        return cachedContainer;
//    }
//
//    private H5EGraphicElement createedColorBackgroundSprite(H5ELayer layer) {
//        H5ESpriteType imageSpriteType = layer.getEngine().getResourceManager().getSpriteType("shipyard1");
//        H5ESprite sprite = new H5ESprite(layer, imageSpriteType);
//        sprite.setShadowColor("red");
//        sprite.setShadowBlur(2);
//        sprite.setTransparency(0.5);
//        return sprite;
//    }
//
//    private H5EGraphicElement createSingleColorSprite(H5ELayer layer) {
//        H5ECachedContainer cachedContainer = new H5ECachedContainer(layer);
//        H5ESpriteType imageSpriteType = layer.getEngine().getResourceManager().getSpriteType("shipyard1");
//        H5ESprite bottomLayer = new H5ESprite(layer, imageSpriteType);
//        H5ERectangle topLayer = new H5ERectangle(layer);
//        topLayer.setColor(Color.valueOf("red"));
//        topLayer.setWidth(bottomLayer.getWidth());
//        topLayer.setHeight(bottomLayer.getHeight());
//        topLayer.setBlendingMode(BlendingMode.SOURCE_ATOP);
//        cachedContainer.addChild(bottomLayer);
//        cachedContainer.addChild(topLayer);
//        return cachedContainer;
//    }

    @Override
    public void open() {
        controlWindow.open();
    }

    @Override
    public void close() {
        controlWindow.close();
    }

    private void cleanup() {
        while (!openDialogs.isEmpty()) {
            openDialogs.pollFirst().close();
        }
    }

    @Override
    public boolean isOpen() {
        return controlWindow.isOpen();
    }

}
