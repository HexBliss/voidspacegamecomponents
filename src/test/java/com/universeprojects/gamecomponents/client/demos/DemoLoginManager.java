package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.gamecomponents.client.dialogs.login.CharacterInfo;
import com.universeprojects.gamecomponents.client.dialogs.login.GCLoginManager;
import com.universeprojects.gamecomponents.client.dialogs.login.LoginInfo;
import com.universeprojects.gamecomponents.client.dialogs.login.ServerInfo;
import com.universeprojects.html5engine.client.framework.H5ELayer;

import java.util.ArrayList;
import java.util.List;

public class DemoLoginManager extends GCLoginManager<DemoLoginManager.DemoLoginInfo, DemoLoginManager.DemoServerInfo, DemoLoginManager.DemoCharacterInfo> {
    public DemoLoginManager(H5ELayer layer) {
        super(layer);
    }

    @Override
    protected void login(String email, String password, Callable1Args<DemoLoginInfo> successCallback, Callable1Args<String> errorCallback) {
        successCallback.call(new DemoLoginInfo());
    }

    @Override
    protected void register(String email, String password, String userName, Callable1Args<DemoLoginInfo> successCallback, Callable1Args<String> errorCallback) {
        successCallback.call(new DemoLoginInfo());
    }

    @Override
    protected void loadPrivacyPolicy(Callable1Args<String> successCallback, Callable1Args<String> errorCallback) {
        successCallback.call("PRIVACY");
    }

    @Override
    protected void loadTermsOfService(Callable1Args<String> successCallback, Callable1Args<String> errorCallback) {
        successCallback.call("TOS");
    }

    @Override
    protected void loadServers(DemoLoginInfo loginInfo, Callable1Args<List<DemoServerInfo>> successCallback, Callable1Args<String> errorCallback) {
        List<DemoServerInfo> list = new ArrayList<>();
        for(int i=1;i<=20;i++) {
            list.add(new DemoServerInfo("Server "+i));
        }
        successCallback.call(list);
    }

    @Override
    protected void pingServer(DemoServerInfo serverInfo, Callable0Args successCallback, Callable1Args<String> errorCallback) {
        successCallback.call();
    }

    @Override
    protected void loadGameCharacters(DemoLoginInfo loginInfo, DemoServerInfo serverInfo, Callable1Args<List<DemoCharacterInfo>> successCallback, Callable1Args<String> errorCallback) {
        List<DemoCharacterInfo> list = new ArrayList<>();
        for(int i=1;i<=5;i++) {
            list.add(new DemoCharacterInfo("InGame "+i));
        }
        successCallback.call(list);
    }

    @Override
    protected void loadUserCharacters(DemoLoginInfo loginInfo, Callable1Args<List<String>> successCallback, Callable1Args<String> errorCallback) {
        List<String> list = new ArrayList<>();
        for(int i=1;i<=5;i++) {
            list.add("NotInGame "+i);
        }
        successCallback.call(list);
    }

    @Override
    protected void deleteCharacter(DemoLoginInfo loginInfo, DemoServerInfo serverInfo, DemoCharacterInfo characterInfo, Callable0Args successCallback, Callable1Args<String> errorCallback) {
        successCallback.call();
    }

    @Override
    protected void joinWithNewCharacter(DemoLoginInfo loginInfo, DemoServerInfo serverInfo, String characterName, Callable0Args successCallback, Callable1Args<String> errorCallback) {
        successCallback.call();
    }

    @Override
    protected void joinWithExistingCharacter(DemoLoginInfo loginInfo, DemoServerInfo serverInfo, DemoCharacterInfo characterInfo, Callable0Args successCallback, Callable1Args<String> errorCallback) {
        successCallback.call();
    }

    public static class DemoLoginInfo implements LoginInfo {

    }

    public static class DemoServerInfo implements ServerInfo {

        private final String name;

        public DemoServerInfo(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public int getPopulation() {
            return 3;
        }

        @Override
        public Integer getMaxPopulation() {
            return 8;
        }
    }

    public static class DemoCharacterInfo implements CharacterInfo {

        private final String name;

        public DemoCharacterInfo(String name) {
            this.name = name;
        }


        @Override
        public String getName() {
            return name;
        }
    }
}
