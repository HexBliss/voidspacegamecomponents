package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCIdeasData;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCInventionSystemDialog;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCKnowledgeData;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCRecipeData;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCRecipeData.GCRecipeSlotDataItem;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCSkillsData;
import com.universeprojects.gamecomponents.client.windows.Alignment;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gamecomponents.client.windows.Position;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

import java.util.Random;

public class InventionSystemDemo extends Demo {

    private final GCSimpleWindow controlWindow;
    private final H5EButton btnToggleAnimation;
    private boolean animate = false;

    private final InventionDialogDemoImpl inventionDialog;

    public InventionSystemDemo(H5ELayer layer) {
        controlWindow = new GCSimpleWindow(layer, "invention-system-demo", "Invention Dialog Demo");
        controlWindow.onClose.registerHandler(this::cleanup);
        controlWindow.positionProportionally(.6f, .15f);

        btnToggleAnimation = H5EButton.createRectBacked(layer, "Turn animation " + (animate ? "OFF" : "ON"));
        btnToggleAnimation.addButtonListener(() -> {
            animate = !animate;
            btnToggleAnimation.setText("Turn animation " + (animate ? "OFF" : "ON"));
            controlWindow.activate();
        });
        controlWindow.addElement(btnToggleAnimation, Position.SAME_LINE, Alignment.LEFT, 0, 0);

        inventionDialog = new InventionDialogDemoImpl(layer);
        inventionDialog.positionProportionally(.65f, .6f);
    }

    @Override
    public void open() {
        controlWindow.open();
        inventionDialog.open();
    }

    @Override
    public void close() {
        controlWindow.close();
    }

    private void cleanup() {
        inventionDialog.close();
    }

    @Override
    public boolean isOpen() {
        return inventionDialog.isOpen();
    }

    @Override
    public void animate() {
        if (animate) {
            inventionDialog.animate();
        }
    }

    private static class InventionDialogDemoImpl extends GCInventionSystemDialog {

        private Logger log = Logger.getLogger(InventionDialogDemoImpl.class);

        public InventionDialogDemoImpl(H5ELayer layer) {
            super(layer);
        }

        private long lastAnimationFrame = System.currentTimeMillis();

        @Override
        public void loadDataForKnowledgeTab() {
            flagLoading();
            GCKnowledgeData knowledgeData = GCKnowledgeData.createNew()
                .add("Hydrogen", 155)
                .add("Oxygen", 389)
                .add("Nitrogen", 38)
                .add("Argon", 3)
                .add("Titanium", 7)
                .add("Aluminium", 11)
                .add("Duralumin", 96)
                .add("Stainless Steel", 128)
                .add("Metal Debris", 552)
                .add("Space Garbage", 993)
                .add("Asteroid Rock", 430)
                .add("Space Dust", 430)
                .add("Standard Missile", 21)
                .add("Plasma Blaster", 107)
                .add("Missile Launcher", 90)
                .add("Escape Pod", 3);

            openKnowledgeTab(knowledgeData);
        }

        @Override
        public void loadDataForExperimentsTab() {
            flagLoading();
            GCInventoryData experimentsData = GCInventoryData.createNew()
                .add("1.hydrogen", "Hydrogen", "hydrogen-icon", 1)
                .add("2.nitrogen", "Nitrogen", "hydrogen-icon", 1)
                .add("3.titanium", "Titanium", "metal-icon", 1)
                .add("4.aluminium", "Aluminium", "metal-icon", 1)
                .add("5.duralumin", "Duralumin", "metal-icon", 1)
                .add("6.standard.missile", "Standard Missile", "missile1-icon", 1)
                .add("7.plasma.blaster", "Plasma Blaster", "turret1-icon", 1)
                .add("8.escape.pod", "Escape Pod", "drone1-icon", 1)
                .add("9.space.garbage", "Space Garbage", "drone1-icon", 1)
                .add("10.metal.debris", "Metal Debris", "drone1-icon", 1);

            openExperimentsTab(experimentsData);
        }

        @Override
        public void loadDataForIdeasTab() {
            flagLoading();
            GCIdeasData ideasData = GCIdeasData.createNew()
                .add(100L, "Timed-activation Mine", "An explosive device with an activation timer", "unknown-icon")
                .add(101L, "Proximity Mine", "An explosive device with a proximity sensor to trigger the explosion", "unknown-icon")
                .add(102L, "Bozo Mine", "An explosive device with a speaker, that plays a circus song for 5 seconds before the explosion is triggered", "unknown-icon");

            openIdeasTab(ideasData);
        }

        @Override
        public void loadDataForItemsTab() {
            flagLoading();
            GCSkillsData skillsData = GCSkillsData.createNew()
                .add(100L, "Timed-activation Mine", "An explosive device with an activation timer", "unknown-icon");

            openItemsTab(skillsData);
        }

        @Override
        public void loadDataForBuildingsTab() {
            // TODO: implement
            flagLoading();
            openBuildingsTab(null);
        }


        void animate() {
            if (System.currentTimeMillis() - lastAnimationFrame < 1000) {
                return;
            }
            lastAnimationFrame = System.currentTimeMillis();

            if (!isCurrentTab(recipeTab)) {
                return;
            }

            for (GCRecipeSlotDataItem slot : recipe.getSlots()) {
                GCInventoryData updatedItems = GCInventoryData.createNew();
                GCInventoryData existingItems = slot.getItems();
                existingItems.forEach((item) -> {
                    if (item.quantity > 1) {
                        updatedItems.add(item.uid, item.name, item.iconSpriteKey, item.quantity - 1);
                    }
                });
                slot.replaceItems(updatedItems);
            }

            updateRecipeTab(recipe);
        }


        private int quantity() {
            return random(3, random(7, 45));
        }

        private int random(int min, int max) {
            return min + rnd.nextInt(max + 1);
        }

        private Random rnd = new Random();
        private GCRecipeData recipe = null;

        private void initRecipeData() {
            final boolean REQUIRED = true, OPTIONAL = false;

            recipe = GCRecipeData.createNew(GCRecipeData.RecipeMode.ITEM_IDEA, 100L)

                .addSlot("0.container",
                    "Container",
                    REQUIRED,
                    null,
                    GCInventoryData.createNew()
                        .add("1.gas.cylinder", "Gas cylinder", "drone1-icon", quantity())
                        .add("2.shoe.box", "Shoe box", "drone1-icon", quantity())
                        .add("3.beer.bottle", "Beer bottle", "drone1-icon", quantity()))

                .addSlot("1.flammable.gas",
                    "A flammable gas",
                    REQUIRED,
                    null,
                    GCInventoryData.createNew()
                        .add("1.hydrogen", "Hydrogen", "hydrogen-icon", quantity())
                        .add("2.nitrogen", "Nitrogen", "hydrogen-icon", quantity())
                        .add("3.dihydrogen.sulfide", "Dihydrogen sulfide", "hydrogen-icon", quantity()))


                .addSlot("2.firestarter",
                    "Any fire-starter",
                    REQUIRED,
                    null,
                    GCInventoryData.createNew()
                        .add("1.lighter", "Lighter", "drone1-icon", quantity())
                        .add("2.box.of.matches", "Box of matches", "drone1-icon", quantity()))

                .addSlot("3.sticker",
                    "A funny sticker",
                    OPTIONAL,
                    null,
                    GCInventoryData.createNew()
                        .add("1.hello.kitty.sticker", "Hello Kitty Sticker", "hello-kitty-icon", quantity()))

                .addSlot("4.spices",
                    "Some spice to taste",
                    OPTIONAL,
                    null,
                    GCInventoryData.createNew()
                        .add("0.sea.salt", "Sea salt", "unknown-icon", quantity())
                        .add("1.black.pepper", "Black pepper", "unknown-icon", quantity())
                        .add("2.cumin", "Cumin", "unknown-icon", quantity())
                        .add("3.paprica", "Paprica", "unknown-icon", quantity())
                        .add("4.tabasco", "Tabasco", "unknown-icon", quantity())
                        .add("5.garlic.powder", "Garlic powder", "unknown-icon", quantity())
                        .add("6.red.curry.paste", "Red curry paste", "unknown-icon", quantity())
                        .add("7.dried.basil", "Dried basil", "unknown-icon", quantity())
                        .add("8.oregano", "Oregano", "unknown-icon", quantity())
                        .add("9.dried.basil", "Dried basil", "unknown-icon", quantity())
                        .add("10.clove", "Clove", "unknown-icon", quantity())
                        .add("11.anise.seed", "Anise seed", "unknown-icon", quantity())
                        .add("12.green.cardamom.seed", "Green cardamom seed", "unknown-icon", quantity())
                        .add("13.black.cardamom.seed", "Black cardamom seed", "unknown-icon", quantity()));
        }


        @Override
        public void onBeginExperimentsBtnPressed(Integer selectionIdx, int repeats) {
            log.info("Begin-Experiments button pressed with selection index " + selectionIdx + " and repeats " + repeats + " time(s).");
        }

        @Override
        public void onBeginPrototypingBtnPressed(Long ideaId) {
            log.info("Begin-Prototyping button pressed with selection index " + ideaId);
            if (ideaId == null) {
                log.info("Please make a selection");
                return;
            }
            initRecipeData();
            openRecipeTab(recipe);
        }

        @Override
        public void onBuildItemBtnPressed(Long skillId) {
            log.info("Build-Item button pressed with skill id: " + skillId);
            initRecipeData();
            openRecipeTab(recipe);
        }

        @Override
        public void onDeleteSkillBtnPressed(Long skillId) {
            log.info("Delete-Skill button pressed with skill id: " + skillId);
        }

        @Override
        public void onBuildBuildingBtnPressed(Long skillId) {
            log.info("Build-Building button pressed with skill id: " + skillId);
        }

        @Override
        public void onRecipeOkayBtnPressed(GCRecipeData recipe, int repeats) {
            log.info("Recipe-Okay button pressed with recipe state: " + recipe.getReferenceId() + " and " + repeats + " repeats");
        }

        @Override
        public void onIconInfoClicked(String uid) {
            log.info("Info icon button pressed with uid: " + uid);
        }

        @Override
        public void onCancelBtnPressed() {
            log.info("Cancel button pressed");
        }

        @Override
        public void showCancelBtn() {

        }

        @Override
        public void hideCancelBtn() {

        }

        @Override
        public void showEmbeddedProgressBar() {

        }

        @Override
        public void hideEmbeddedProgressBar() {

        }

        @Override
        public void setEmbeddedProgressBarMaximum(int maximum) {

        }

        @Override
        public void setEmbeddedProgressBarProgress(int progress) {

        }

        @Override
        public void clearEmbeddedProgressBar() {

        }
    }
}
