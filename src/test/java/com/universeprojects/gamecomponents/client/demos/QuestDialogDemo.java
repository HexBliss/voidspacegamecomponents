package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.gamecomponents.client.dialogs.quests.GCQuestDialog;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class QuestDialogDemo extends Demo {

    private final GCQuestDialog inspector;

    public QuestDialogDemo(H5ELayer layer) {
        inspector = new GCQuestDialog(layer);
        inspector.positionProportionally(0.7f, 0.5f);
    }

    @Override
    public void open() {
        inspector.open();
    }

    @Override
    public void close() {
        inspector.close();
    }

    @Override
    public boolean isOpen() {
        return inspector.isOpen();
    }
}
