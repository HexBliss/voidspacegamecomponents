package com.universeprojects.gamecomponents.client.demos;

import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.gamecomponents.client.windows.Alignment;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gamecomponents.client.windows.Position;
import com.universeprojects.html5engine.client.framework.H5EContainer;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.LinkedList;
import java.util.List;

public class LabelsDemo extends Demo {

    private final H5ELayer layer;
    private final GCSimpleWindow window;

    private final static int WINDOW_W = 875;
    private final static int WINDOW_H = 450;
    private final static int MAX_LABEL_W = 775;

    private final List<AnimatedLabel> labels = new LinkedList<>();

    private int t = 0;

    public LabelsDemo(H5ELayer layer) {
        this.layer = Dev.checkNotNull(layer);

        window = new GCSimpleWindow(layer, "labels-demo", "Label Dimensions", WINDOW_W, WINDOW_H);
        window.positionProportionally(0.65f, 0.65f);

        window.addLabel("Normal labels:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        window.addEmptyLine(5);
        for (float size = 0.4f; size <= 1.2f; size += 0.1f) {
            addLabel(size);
        }

        window.addEmptyLine(25);

        window.addLabel("Bold labels:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        final Label.LabelStyle boldStyle = window.getSkin().get("label-bold", Label.LabelStyle.class);
        window.addEmptyLine(5);
        for (float size = 0.4f; size <= 1.2f; size += 0.1f) {
            AnimatedLabel label = addLabel(size);
            label.label.setStyle(boldStyle);
        }
    }

    private AnimatedLabel addLabel(float fontScale) {
        AnimatedLabel label = new AnimatedLabel(layer, fontScale);
        labels.add(label);
        window.addElement(label, Position.NEW_LINE, Alignment.LEFT, 0, 0);
        Cell<AnimatedLabel> cell = window.getCell(label);
        cell.expandX();
        return label;
    }

    @Override
    public void open() {
        window.open(false);
    }

    @Override
    public void close() {
        window.close();
        for (AnimatedLabel label : labels) {
            label.resetLabel();
        }
    }

    @Override
    public boolean isOpen() {
        return window.isOpen();
    }

    @Override
    public void animate() {
        if (++t % 3 != 0) {
            return;
        }

        for (AnimatedLabel label : labels) {
            label.animate();
        }
    }

    private static class AnimatedLabel extends H5EContainer {

        private final H5ELabel label;

        private final String PANGRAMS = "The quick brown fox jumps over the lazy dog. The five boxing wizards jump quickly. Sphinx of black quartz, judge my vow.";

        private int beginIndex = 0;
        private int endIndex = 1;

        AnimatedLabel(H5ELayer layer, float fontScale) {
            super(layer);

            label = new H5ELabel(layer);
            Cell<H5ELabel> labelCell = add(label);
            labelCell.growX();
            label.setText("[");
            label.setFontScale(fontScale);
            label.setFillParent(true);

            setHeight(label.getHeight());

            resetLabel();
        }

        void resetLabel() {
            label.setText("[");

            beginIndex = 0;
            endIndex = 1;
        }

        void animate() {
            label.setText(PANGRAMS.substring(beginIndex, endIndex));
            label.layout();

            final int labelW = (int) label.getWidth();

            if (endIndex < PANGRAMS.length() - 1) {
                endIndex++;
                if (labelW >= LabelsDemo.MAX_LABEL_W) {
                    beginIndex++;
                }
            } else {
                beginIndex++;
                if (endIndex - beginIndex < 1) {
                    beginIndex = 0;
                    endIndex = 1;
                }
            }
        }
    }


}
