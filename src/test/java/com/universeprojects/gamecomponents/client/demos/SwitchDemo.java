package com.universeprojects.gamecomponents.client.demos;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.gamecomponents.client.elements.GCSwitch;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class SwitchDemo extends Demo {

    private final GCSimpleWindow window;

    private GCSwitch switchG, switchR, switchW;
    private final H5ELabel labelG, labelR, labelW;


    public SwitchDemo(H5ELayer layer) {
        window = new GCSimpleWindow(layer, "switch-demo", "Switch Demo");
        window.positionProportionally(.5f, .5f);

        window.defaults().left();

        window.addH2("Green switch:");
        switchG = new GCSwitch(layer);
        switchG.addButtonListener(this::updateStatusLabels);
        window.add(switchG).right().colspan(2);

        window.addEmptyLine(15);
        window.addH2("Red switch:");
        switchR = new GCSwitch(layer);
        switchR.addButtonListener(this::updateStatusLabels);
        window.add(switchR).right().colspan(2);

        window.addEmptyLine(15);
        window.addH2("White switch:");
        switchW = new GCSwitch(layer);
        switchW.addButtonListener(this::updateStatusLabels);
        window.add(switchW).right().colspan(2);

        window.addEmptyLine(25);

        labelG = new H5ELabel(layer);
        labelG.setColor(Color.valueOf("#00FF00"));

        labelR = new H5ELabel(layer);
        labelR.setColor(Color.valueOf("#FF0000"));

        labelW = new H5ELabel(layer);
        labelW.setColor(Color.valueOf("#FFFFFF"));

        Table table = new Table();
        table.defaults().left().size(120, 30);
        table.add(labelG);
        table.add(labelR);
        table.add(labelW);

        window.add(table).colspan(3);

        updateStatusLabels();
    }

    private static String getStatus(GCSwitch sw) {
        return sw.isChecked() ? "ON" : "OFF";
    }

    @Override
    public void open() {
        window.open();
    }

    @Override
    public void close() {
        window.close();
    }

    @Override
    public boolean isOpen() {
        return window.isOpen();
    }

    private void updateStatusLabels() {
        labelG.setText("Green: " + getStatus(switchG));
        labelR.setText("Red: " + getStatus(switchR));
        labelW.setText("White: " + getStatus(switchW));
    }
}
