package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.gamecomponents.client.dialogs.GCStackSplitter;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class StackSplitterDemo extends Demo {

    private final Logger log = Logger.getLogger(StackSplitterDemo.class);

    private GCStackSplitter splitter;

    public StackSplitterDemo(H5ELayer layer) {
        splitter = new GCDialogStackSplitterImpl(layer);
        splitter.positionProportionally(0.7f, 0.5f);

    }

    @Override
    public void open() {
        splitter.open();
    }

    @Override
    public void close() {
        splitter.close();
    }

    @Override
    public boolean isOpen() {
        return splitter.isOpen();
    }

    public class GCDialogStackSplitterImpl extends GCStackSplitter {

        public GCDialogStackSplitterImpl(H5ELayer layer) {
            super(layer, "hydrogen-icon", "Test Object Name", 100);
        }

        @Override
        public void onSubmit(int amountToSplit) {
            log.info("Stack splitter dialog submitted with amount = " + amountToSplit);
        }

        @Override
        public void onClose() {
            log.info("Stack splitter dialog is now closed");
        }

    }

}
