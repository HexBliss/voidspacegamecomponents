package com.universeprojects.gamecomponents.client.selection;

public interface GCSelectionMenuActionHandler {

    void executeAction(String path);

}
