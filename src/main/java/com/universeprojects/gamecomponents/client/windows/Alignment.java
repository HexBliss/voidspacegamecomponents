package com.universeprojects.gamecomponents.client.windows;

import com.badlogic.gdx.utils.Align;

/**
 * Possible alignment values used with SimpleWindow elements
 */
public enum Alignment {
    LEFT(Align.left), CENTER(Align.center), RIGHT(Align.right);

    private final int value;

    Alignment(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
