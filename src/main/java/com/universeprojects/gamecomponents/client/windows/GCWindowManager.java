package com.universeprojects.gamecomponents.client.windows;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.html5engine.client.framework.H5EEngine;

import java.util.Deque;
import java.util.LinkedList;

public class GCWindowManager {

    public static GCWindowManager getInstance(H5EEngine engine) {
        return engine.getOrCreateEngineBoundObject(GCWindowManager.class.getCanonicalName(), () -> new GCWindowManager(engine));
    }

    /**
     * This deque contains references to all the windows that are currently open.
     * The first (head) entry references the "active" window.
     */
    private Deque<GCWindow> openWindows = new LinkedList<>();

    public GCWindowManager(H5EEngine engine) {
        //TODO put this into GameControls or find a better place later
        engine.addInputProcessor(new InputAdapter() {
            @Override
            public boolean keyUp(int keycode) {
                if(keycode == Input.Keys.ESCAPE) {
                    final GCWindow window = openWindows.peekFirst();
                    if(window != null && window.isCloseButtonEnabled()) {
                        window.close();
                    }
                }
                return super.keyUp(keycode);
            }
        });
    }

    /**
     * Closes all the open windows
     */
    public final void closeAll() {
        while (openWindows.peekFirst() != null) {
            GCWindow topWindow = openWindows.getFirst();
            topWindow.close(true);
        }
    }

    /**
     * @return TRUE if the given window is active, FALSE otherwise
     */
    final boolean isActive(GCWindow window) {
        Dev.checkNotNull(window);
        return !openWindows.isEmpty() && window == openWindows.getFirst();
    }

    /**
     * This method is called to signal that the given window is to become the "active" window.
     * This will happen when a new window is opened, or when an existing inactive window is clicked.
     */
    final void activateWindow(GCWindow window) {
        Dev.checkNotNull(window);
        if (!openWindows.isEmpty()) {
            GCWindow activeWindow = openWindows.getFirst();
            if (activeWindow == window) {
                // do nothing - window is already active
                return;
            } else {
                // deactivate the window that's currently active
                activeWindow.setupInactiveState();
            }
        }

        // activate the new window, and place it at the head of the deque
        window.setupActiveState();
        openWindows.remove(window);
        openWindows.addFirst(window);
    }

    /**
     * This method is called to signal that a given window is no longer open.
     * The window will be removed from the list of open windows. If it was the "active" window and if there are
     * other open windows, the most recently-active open window will become the "active" window.
     */
    final void windowClosed(GCWindow window) {
        Dev.checkNotNull(window);
        if (!openWindows.contains(window)) {
            // looks like this was already done - ignore this call
            return;
        }

        if (openWindows.getFirst() != window) {
            // this window is not the active window.. simply remove it from the list
            openWindows.remove(window);
            return;
        }

        // this window was the active window
        window.setupInactiveState();
        openWindows.removeFirst();
        // if more open windows are left, pick the next one to become active (it would be the most recently-active)
        if (!openWindows.isEmpty()) {
            openWindows.getFirst().setupActiveState();
        }
    }

}
