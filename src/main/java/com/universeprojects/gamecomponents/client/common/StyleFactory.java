package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Cursor;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TransformDrawable;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5EResourceManager;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.client.framework.drawable.RectShapeDrawable;
import com.universeprojects.html5engine.client.framework.drawable.ScaledDrawable;

public class StyleFactory {
    public static final String RESOURCE_DEFAULT = "default";
    public static final String RESOURE_DEFAULT_HORIZONTAL = "default-horizontal";
    public static final String RESOURCE_DEFAULT_VERTICAL = "default-vertical";

    private final H5EResourceManager rm;
    private final TextureAtlas atlas;

    public StyleFactory(H5EResourceManager rm, TextureAtlas atlas) {
        this.rm = rm;
        this.atlas = atlas;
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    public Skin generateSkin() {
        Skin skin = new Skin();

        BitmapFont fontElectrolize = createFont("fonts/Electrolize/electrolize.fnt");
        skin.add("font-electrolize", fontElectrolize);
        BitmapFont fontElectrolizeBold = createFont("fonts/Electrolize/electrolize-bold.fnt");
        skin.add("font-electrolize-bold", fontElectrolizeBold);

        BitmapFont fontLaconic = createFont("fonts/Laconic/laconic-regular.fnt");
        skin.add("font-laconic", fontLaconic);
        BitmapFont fontLaconicBold = createFont("fonts/Laconic/laconic-bold.fnt");
        skin.add("font-laconic-bold", fontLaconicBold);
        BitmapFont fontLaconicLight = createFont("fonts/Laconic/laconic-light.fnt");
        skin.add("font-laconic-light", fontLaconicLight);

        BitmapFont fontArialSmall = createFont("fonts/Arial/small.fnt");
        skin.add("font-arial-small", fontArialSmall);

        BitmapFont font = fontElectrolize;
        skin.add(RESOURCE_DEFAULT, font);

        BitmapFont fontBold = fontElectrolizeBold;
        skin.add("default-bold", fontBold);

        BitmapFont fontTitle = fontLaconic;
        skin.add("default-title", fontTitle);

        BitmapFont fontButtons = fontLaconic;
        skin.add("default-buttons", fontButtons);

        BitmapFont fontSmall = fontArialSmall;
        skin.add("default-small", fontSmall);


        Label.LabelStyle labelStyle = new Label.LabelStyle(font, Color.WHITE);
        skin.add(RESOURCE_DEFAULT, labelStyle);
        Label.LabelStyle boldLabelStyle = new Label.LabelStyle(fontBold, Color.WHITE);
        skin.add("label-bold", boldLabelStyle);
        Label.LabelStyle smallLabelStyle = new Label.LabelStyle(fontSmall, Color.WHITE);
        skin.add("label-small", smallLabelStyle);

        for (TextureRegion region : font.getRegions()) {
            region.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        for (TextureRegion region : fontBold.getRegions()) {
            region.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }


//        final NinePatch mediumButtonPatch = atlas.createPatch("GUI/blue/generic-button-medium");
//        NinePatchDrawable mediumButtonDrawable = new NinePatchDrawable(mediumButtonPatch);
//        ImageTextButton.ImageTextButtonStyle mediumButtonStyle = createButtonStyle(mediumButtonDrawable, font, 1.1f, 2f);
//        skin.add(RESOURCE_DEFAULT, mediumButtonStyle, ImageTextButton.ImageTextButtonStyle.class);
//        skin.add(RESOURCE_DEFAULT, mediumButtonStyle, Button.ButtonStyle.class);


        final NinePatchDrawable ui2ButtonNormalDrawable   = ninePatchDrawable("GUI/ui2/button_wide_normal1");
        final NinePatchDrawable ui2ButtonHoverDrawable    = ninePatchDrawable("GUI/ui2/button_wide_hovered1");
        final NinePatchDrawable ui2ButtonPressedDrawable  = ninePatchDrawable("GUI/ui2/button_wide_pressed1");
        final NinePatchDrawable ui2ButtonDisabledDrawable = ninePatchDrawable("GUI/ui2/button_wide_disabled1");

        ImageTextButton.ImageTextButtonStyle ui2ButtonStyle = new ImageTextButton.ImageTextButtonStyle(ui2ButtonNormalDrawable, ui2ButtonPressedDrawable, ui2ButtonPressedDrawable, fontButtons);
        ui2ButtonStyle.over = ui2ButtonHoverDrawable;
        ui2ButtonStyle.checkedOver = ui2ButtonHoverDrawable;
        ui2ButtonStyle.disabled = ui2ButtonDisabledDrawable;
        skin.add(RESOURCE_DEFAULT, ui2ButtonStyle, ImageTextButton.ImageTextButtonStyle.class);
        skin.add(RESOURCE_DEFAULT, ui2ButtonStyle, Button.ButtonStyle.class);
        skin.add("button-small", ui2ButtonStyle, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button-small", ui2ButtonStyle, Button.ButtonStyle.class);

        final NinePatchDrawable ui2Button2NormalDrawable       = ninePatchDrawable("GUI/ui2/button2_unselected_normal");
        final NinePatchDrawable ui2Button2HoverDrawable        = ninePatchDrawable("GUI/ui2/button2_unselected_hover");
        final NinePatchDrawable ui2Button2CheckedDrawable      = ninePatchDrawable("GUI/ui2/button2_selected_normal");
        final NinePatchDrawable ui2Button2CheckedHoverDrawable = ninePatchDrawable("GUI/ui2/button2_selected_hover");

        ImageTextButton.ImageTextButtonStyle ui2Button2Style = new ImageTextButton.ImageTextButtonStyle(ui2Button2NormalDrawable, ui2Button2CheckedDrawable, null, fontButtons);
        ui2Button2Style.over = ui2Button2HoverDrawable;
        skin.add("button2", ui2Button2Style, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button2", ui2Button2Style, Button.ButtonStyle.class);

        ImageTextButton.ImageTextButtonStyle ui2Button2NoPaddingStyle = new ImageTextButton.ImageTextButtonStyle(ui2Button2NormalDrawable, null, ui2Button2CheckedDrawable, fontButtons);
        ui2Button2NoPaddingStyle.over = ui2Button2HoverDrawable;
        ui2Button2NoPaddingStyle.checkedOver = ui2Button2CheckedHoverDrawable;
        zeroPadding(ui2Button2NoPaddingStyle.up);
        zeroPadding(ui2Button2NoPaddingStyle.checked);
        zeroPadding(ui2Button2NoPaddingStyle.over);
        zeroPadding(ui2Button2NoPaddingStyle.checkedOver);
        skin.add("button2-no-padding", ui2Button2NoPaddingStyle, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button2-no-padding", ui2Button2NoPaddingStyle, Button.ButtonStyle.class);

        final NinePatchDrawable uiTabButtonNormalDrawable       = ninePatchDrawable("GUI/ui2/button2_unselected_normal");
        final NinePatchDrawable uiTabButtonHoverDrawable        = ninePatchDrawable("GUI/ui2/button2_unselected_hover");
        final NinePatchDrawable uiTabButtonCheckedDrawable      = ninePatchDrawable("GUI/ui2/button2_selected_normal");
        final NinePatchDrawable uiTabButtonCheckedHoverDrawable = ninePatchDrawable("GUI/ui2/button2_selected_hover");

        final int TAB_BUTTON_PAD_LEFT   = 11;
        final int TAB_BUTTON_PAD_RIGHT  = 8;
        final int TAB_BUTTON_PAD_TOP    = 6;
        final int TAB_BUTTON_PAD_BOTTOM = 6;

        ImageTextButton.ImageTextButtonStyle uiTabButtonStyle = new ImageTextButton.ImageTextButtonStyle(uiTabButtonNormalDrawable, null, uiTabButtonCheckedDrawable, fontButtons);
        uiTabButtonStyle.over = uiTabButtonHoverDrawable;
        uiTabButtonStyle.checkedOver = uiTabButtonCheckedHoverDrawable;
        uiTabButtonStyle.checkedOffsetY = -0.5f;
        setPadding(uiTabButtonStyle.up,          TAB_BUTTON_PAD_LEFT, TAB_BUTTON_PAD_RIGHT, TAB_BUTTON_PAD_TOP, TAB_BUTTON_PAD_BOTTOM);
        setPadding(uiTabButtonStyle.checked,     TAB_BUTTON_PAD_LEFT, TAB_BUTTON_PAD_RIGHT, TAB_BUTTON_PAD_TOP, TAB_BUTTON_PAD_BOTTOM);
        setPadding(uiTabButtonStyle.over,        TAB_BUTTON_PAD_LEFT, TAB_BUTTON_PAD_RIGHT, TAB_BUTTON_PAD_TOP, TAB_BUTTON_PAD_BOTTOM);
        setPadding(uiTabButtonStyle.checkedOver, TAB_BUTTON_PAD_LEFT, TAB_BUTTON_PAD_RIGHT, TAB_BUTTON_PAD_TOP, TAB_BUTTON_PAD_BOTTOM);
        skin.add("button2-tab", uiTabButtonStyle, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button2-tab", uiTabButtonStyle, Button.ButtonStyle.class);

        final NinePatchDrawable uiButtonRedNormal  = ninePatchDrawable("GUI/ui2/button2-red-normal");
        final NinePatchDrawable uiButtonRedHover   = ninePatchDrawable("GUI/ui2/button2-red-hover");
        final NinePatchDrawable uiButtonRedPressed = ninePatchDrawable("GUI/ui2/button2-red-pressed");

        ImageTextButton.ImageTextButtonStyle ui2ButtonRedStyle = new ImageTextButton.ImageTextButtonStyle(uiButtonRedNormal, uiButtonRedPressed, null, fontButtons);
        ui2ButtonRedStyle.over = uiButtonRedHover;
        skin.add("button-red", ui2ButtonRedStyle, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("button-red", ui2ButtonRedStyle, Button.ButtonStyle.class);

        NinePatchDrawable textFieldBackgroundDrawable = ninePatchDrawable("GUI/ui2/text_input1_background");
        RectShapeDrawable textFieldCursorDrawable = new RectShapeDrawable(Color.WHITE);
        textFieldCursorDrawable.setMinWidth(2);
        RectShapeDrawable textFieldSelectionDrawable = new RectShapeDrawable(Color.CYAN);
        TextField.TextFieldStyle textFieldStyle = new TextField.TextFieldStyle(font, Color.WHITE, textFieldCursorDrawable, textFieldSelectionDrawable, textFieldBackgroundDrawable);
        skin.add(RESOURCE_DEFAULT, textFieldStyle);

        NinePatchDrawable progressBarBackgroundDrawable = new NinePatchDrawable(atlas.createPatch("GUI/ui2/progress_bar1_background"));
        NinePatchDrawable progressBarKnobDrawable = new NinePatchDrawable(atlas.createPatch("GUI/ui2/progress_bar1_fill"));
        progressBarKnobDrawable.setMinWidth(0);

        ProgressBar.ProgressBarStyle progressBarStyle = new ProgressBar.ProgressBarStyle(progressBarBackgroundDrawable, null);
        progressBarStyle.knobBefore = progressBarKnobDrawable;
        skin.add(RESOURE_DEFAULT_HORIZONTAL, progressBarStyle);
        skin.add(RESOURCE_DEFAULT_VERTICAL, progressBarStyle);//TODO

        NinePatchDrawable gaugeKnobDrawable = new NinePatchDrawable(atlas.createPatch("GUI/ui2/progress_bar1_fill_grey"));
        gaugeKnobDrawable.setMinWidth(0);

        ProgressBar.ProgressBarStyle gaugeStyle = new ProgressBar.ProgressBarStyle(progressBarBackgroundDrawable, null);
        gaugeStyle.knobBefore = gaugeKnobDrawable;
        skin.add("gauge-horizontal", gaugeStyle);
        skin.add("gauge-vertical", gaugeStyle);//TODO


        TextureRegionDrawable barBackgroundDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/im/gauge-lowlight-type1"));
        TextureRegionDrawable healthBarDrawable = new ClippingTextureRegionDrawable(atlas.createSprite("GUI/im/hull-gauge-highlight"), false);
        healthBarDrawable.setMinWidth(0);
        TextureRegionDrawable shieldBarDrawable = new ClippingTextureRegionDrawable(atlas.createSprite("GUI/im/shield-gauge-highlight"), true);
        shieldBarDrawable.setMinWidth(0);
        TextureRegionDrawable energyBarDrawable = new ClippingTextureRegionDrawable(atlas.createSprite("GUI/im/energy-gauge-highlight"), true);
        energyBarDrawable.setMinWidth(0);

        ProgressBar.ProgressBarStyle healthBarStyle = new ProgressBar.ProgressBarStyle(barBackgroundDrawable, null);
        healthBarStyle.knobBefore = healthBarDrawable;
        skin.add("health-bar", healthBarStyle);

        ProgressBar.ProgressBarStyle shieldBarStyle = new ProgressBar.ProgressBarStyle(barBackgroundDrawable, null);
        shieldBarStyle.knobBefore = shieldBarDrawable;
        skin.add("shield-bar", shieldBarStyle);

        ProgressBar.ProgressBarStyle energyBarStyle = new ProgressBar.ProgressBarStyle(barBackgroundDrawable, null);
        energyBarStyle.knobBefore = energyBarDrawable;
        skin.add("energy-bar", energyBarStyle);


        NinePatchDrawable sliderBackgroundDrawable = new NinePatchDrawable(atlas.createPatch("GUI/ui2/slider1_background"));
        TextureRegionDrawable sliderKnobDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/slider1-button-normal"));
        TextureRegionDrawable sliderKnobOverDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/slider1-button-hovered"));
        NinePatchDrawable sliderKnobBeforeDrawable = new NinePatchDrawable(atlas.createPatch("GUI/ui2/slider1_fill"));
        sliderKnobBeforeDrawable.setMinWidth(0);
        sliderBackgroundDrawable.setMinWidth(sliderBackgroundDrawable.getPatch().getLeftWidth() + sliderBackgroundDrawable.getPatch().getRightWidth());


        Slider.SliderStyle sliderStyle = new Slider.SliderStyle();
        sliderStyle.background = sliderBackgroundDrawable;
        sliderStyle.knob = sliderKnobDrawable;
        sliderStyle.knob = sliderKnobOverDrawable;
        sliderStyle.knobBefore = sliderKnobBeforeDrawable;
        skin.add(RESOURE_DEFAULT_HORIZONTAL, sliderStyle);
        skin.add(RESOURCE_DEFAULT_VERTICAL, sliderStyle);

        ImageButton.ImageButtonStyle closeButtonStyle = new ImageButton.ImageButtonStyle();
        TextureRegion btnCloseNormalTexture = atlas.createSprite("GUI/ui2/window-menu2-button-close-normal");
        TextureRegion btnCloseHoverTexture = atlas.createSprite("GUI/ui2/window-menu2-button-close-hovered");
        TextureRegion btnClosePressedTexture = atlas.createSprite("GUI/ui2/window-menu2-button-close-pressed");

        closeButtonStyle.imageUp = new TextureRegionDrawable(btnCloseNormalTexture);
        closeButtonStyle.imageDown = new TextureRegionDrawable(btnClosePressedTexture);
        closeButtonStyle.imageOver = new TextureRegionDrawable(btnCloseHoverTexture);
        skin.add("btn-close-window", closeButtonStyle);

        createSwitchStyle(skin, font);

        NinePatchDrawable windowBackground = ninePatchDrawable("GUI/ui2/window2_background");
        NinePatchDrawable titleBarBackground = ninePatchDrawable("GUI/ui2/window_title_background1", false);

        final Color titleFontColor = Color.valueOf("#e0b487");
        final GCWindow.GCWindowStyle windowStyle = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, windowBackground);
        windowStyle.titleBackground = titleBarBackground;
        windowStyle.titleFontColor = titleFontColor;
        windowStyle.labelPadLeft = 30;
        windowStyle.labelPadBottom = 36;
        windowStyle.closeButtonPadRight = 0;
        windowStyle.closeButtonPadTop = -10;
        skin.add(RESOURCE_DEFAULT, windowStyle);
        skin.add(RESOURCE_DEFAULT, windowStyle, Window.WindowStyle.class);

        final NinePatchDrawable ui2WindowBackground = ninePatchDrawable("GUI/ui2/window_menu_background1");
        final GCWindow.GCWindowStyle ui2WindowStyle = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, ui2WindowBackground);
        ui2WindowStyle.labelPadLeft = 10;
        ui2WindowStyle.labelPadBottom = 24;
        ui2WindowStyle.titleFontColor = titleFontColor;
        skin.add("ui2-window", ui2WindowStyle);
        skin.add("ui2-window", ui2WindowStyle, Window.WindowStyle.class);

        final NinePatchDrawable inspectorWindowBackground = ninePatchDrawable("GUI/ui2/window3_background");
        final GCWindow.GCWindowStyle inspectorWindowStyle = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, inspectorWindowBackground);
        inspectorWindowStyle.closeButtonPadRight = -10;
        inspectorWindowStyle.closeButtonPadTop = 10;
        inspectorWindowStyle.titleFontColor = titleFontColor;
        skin.add("inspector-window", inspectorWindowStyle);
        skin.add("inspector-window", inspectorWindowStyle, Window.WindowStyle.class);

        final NinePatchDrawable loginScreenBackground = ninePatchDrawable("GUI/ui2/loginpanel1");
        final GCWindow.GCWindowStyle loginScreenStyle = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, loginScreenBackground);

        skin.add("login-window", loginScreenStyle);
        skin.add("login-window", loginScreenStyle, Window.WindowStyle.class);

        final NinePatchDrawable ui2InventionWindowBackground = new NinePatchDrawable(atlas.createPatch("GUI/ui2/window4_background"));
//        setupMinSize(ui2InventionWindowBackground);
        ui2InventionWindowBackground.setMinHeight(ui2InventionWindowBackground.getPatch().getTopHeight() + ui2InventionWindowBackground.getPatch().getBottomHeight());
        final GCWindow.GCWindowStyle ui2InventionWindowStyle = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, ui2InventionWindowBackground);
        ui2InventionWindowStyle.closeButtonPadTop = 11;
        ui2InventionWindowStyle.closeButtonPadRight = -32;
        skin.add("ui2-invention-window", ui2InventionWindowStyle);
        skin.add("ui2-invention-window", ui2InventionWindowStyle, Window.WindowStyle.class);

        TextureRegionDrawable selectionMenuBackgroundDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/selector/menu-background-gradient"));

        final Window.WindowStyle selectionMenuWindowStyle = new Window.WindowStyle(fontTitle, Color.WHITE, new ScaledDrawable<>(selectionMenuBackgroundDrawable, 1.4f, 1.8f));
//        selectionMenuWindowStyle.titleFontColor = titleFontColor;
        skin.add("selection-menu-window", selectionMenuWindowStyle);

        final TextureRegionDrawable selectionMenuCloseBtnDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/selector/menu-close"));
        final TextureRegionDrawable selectionMenuUpBtnDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/selector/menu-up-arrow"));

        final ImageButton.ImageButtonStyle selectionMenuCloseBtnStyle = createImageButtonStyle(selectionMenuCloseBtnDrawable, 1.1f, 1f);
        final ImageButton.ImageButtonStyle selectionMenuUpBtnStyle = createImageButtonStyle(selectionMenuUpBtnDrawable, 1.1f, 1f);
        skin.add("selection-menu-close-btn", selectionMenuCloseBtnStyle);
        skin.add("selection-menu-up-btn", selectionMenuUpBtnStyle);

        final TextureRegionDrawable selectionMenuArrowDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/selector/menu-selector-arrow"));
        skin.add("selection-menu-arrow", selectionMenuArrowDrawable, Drawable.class);
        final TextureRegionDrawable selectionMenuSubmenuArrowDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/selector/menu-submenu-arrow"));
        skin.add("selection-menu-submenu-arrow", selectionMenuSubmenuArrowDrawable, Drawable.class);

        H5ESpriteType actionProgressBarBackground = rm.getSpriteType("images/cloudWhite.png");
        final Color color = new Color(Color.WHITE);
        color.a = 0.3f;
        TextureRegionDrawable actionProgressBarBackgroundDrawable = new TextureRegionDrawable(actionProgressBarBackground.getGraphicData()) {
            @Override
            public void draw(Batch batch, float x, float y, float width, float height) {
                Color oldColor = batch.getColor();
                batch.setColor(color);
                super.draw(batch, x, y, width, height);
                batch.setColor(oldColor);
            }

            @Override
            public void draw(Batch batch, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation) {
                Color oldColor = batch.getColor();
                batch.setColor(color);
                super.draw(batch, x, y, originX, originY, width, height, scaleX, scaleY, rotation);
                batch.setColor(oldColor);
            }
        };
        final Texture cloudTexture = actionProgressBarBackground.getGraphicData().getTexture();
        cloudTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        actionProgressBarBackgroundDrawable.setMinWidth(200);
        actionProgressBarBackgroundDrawable.setMinHeight(200);

        Window.WindowStyle actionProgressBarWindowStyle = new Window.WindowStyle(font, Color.WHITE, actionProgressBarBackgroundDrawable);
        skin.add("action-progress-bar-window", actionProgressBarWindowStyle);

        final GCWindow.GCWindowStyle GCactionProgressBarWindowStyle = new GCWindow.GCWindowStyle(fontTitle, Color.WHITE, actionProgressBarBackgroundDrawable);
        skin.add("action-progress-bar-window", GCactionProgressBarWindowStyle,Window.WindowStyle.class);
        
        NinePatchDrawable scrollBackgroundVertical = ninePatchDrawable("GUI/ui2/scrollbar1_backing");
        NinePatchDrawable scrollKnobVertical = ninePatchDrawable("GUI/ui2/scrollbar1_slider");
        ScrollPane.ScrollPaneStyle scrollPaneStyle = new ScrollPane.ScrollPaneStyle();
        scrollPaneStyle.vScroll = scrollBackgroundVertical;
        scrollPaneStyle.vScrollKnob = scrollKnobVertical;
        skin.add(RESOURCE_DEFAULT, scrollPaneStyle);

        ScrollPane.ScrollPaneStyle scrollPaneBackStyle = new ScrollPane.ScrollPaneStyle(scrollPaneStyle);
        scrollPaneBackStyle.background = ninePatchDrawable("GUI/ui2/internal_panel4_background");
        skin.add("scrollpane-backing-1", scrollPaneBackStyle);

        ScrollPane.ScrollPaneStyle scrollPaneBackStyle2 = new ScrollPane.ScrollPaneStyle(scrollPaneStyle);
        scrollPaneBackStyle2.background = ninePatchDrawable("GUI/ui2/internal_panel1_background");
        skin.add("scrollpane-backing-2", scrollPaneBackStyle2);

        NinePatchDrawable gcListItemNormal = ninePatchDrawable("GUI/ui2/internal_panel3_background");
        NinePatchDrawable gcListItemChecked = ninePatchDrawable("GUI/ui2/internal_panel3_background_selected");
        NinePatchDrawable gcListItemHover = ninePatchDrawable("GUI/ui2/internal_panel3_background_highlight");
        Button.ButtonStyle gcListItemStyle = new Button.ButtonStyle(gcListItemNormal, null, gcListItemChecked);
        gcListItemStyle.over = gcListItemHover;
        skin.add("gc-list-item", gcListItemStyle);

        skin.add("gc-list-blank", new Button.ButtonStyle());

       // NinePatchDrawable dropDownNormal = ninePatchDrawable("GUI/ui2/dropdown1_normal");

        final TextureRegionDrawable loginBackgroundDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/preload/startup-background1"));
        skin.add("login-background", loginBackgroundDrawable, Drawable.class);

        final TextureRegionDrawable loginLogoDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/startup-logo1"));
        skin.add("login-logo", loginLogoDrawable, Drawable.class);

        final TextureRegionDrawable chracterEquipmentDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/character-equipment1"));
        skin.add("character-equipment", chracterEquipmentDrawable, Drawable.class);


        NinePatchDrawable loginBorder = ninePatchDrawable("GUI/ui2/startup-lightborders1");
       // NinePatchDrawable loginBorder =new NinePatchDrawable(atlas.createPatch("GUI/ui2/startup-lightborders1"));

        skin.add("login-border",loginBorder, Drawable.class);

        final TextureRegionDrawable statusMessageBackgroundDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/announcement1-background"));
        skin.add("status-message-background", statusMessageBackgroundDrawable, Drawable.class);

        /*
        final TextureRegionDrawable loginBorderDrawable = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/startup-lightborders1"));
        skin.add("login-border", loginBorderDrawable, Drawable.class);
        */

        final TextureRegionDrawable gameBackgroundDrawable = new TextureRegionDrawable(atlas.createSprite("game-background4"));
        skin.add("game-background", gameBackgroundDrawable, Drawable.class);

        final TextureRegionDrawable mapBackgroundDrawable = new TextureRegionDrawable(atlas.createSprite("game-background4"));
        skin.add("map-background", mapBackgroundDrawable, Drawable.class);

        NinePatchDrawable mapButtonNormalDrawable = ninePatchDrawable("map-indicators/button-base");

        ImageTextButton.ImageTextButtonStyle mapButtonStyle = createButtonStyle(mapButtonNormalDrawable, fontBold, 1.2f, 2f);
        skin.add("button-map", mapButtonStyle, ImageTextButton.ImageTextButtonStyle.class);

        final Pixmap cursorPixmap = new Pixmap(Gdx.files.internal("images/GUI/ui2/preload/cursor1-normal.png"));
        Cursor cursor = Gdx.graphics.newCursor(cursorPixmap, 3, 3);
        skin.add(RESOURCE_DEFAULT, cursor, Cursor.class);
//        NinePatchDrawable smallWindowBackground = new ninePatchDrawable("GUI/blue/window-small");
//        Window.WindowStyle smallWindowStyle = new Window.WindowStyle(font, Color.BLACK, smallWindowBackground);
//        skin.add("window-small", smallWindowStyle);

        String[] icons = new String[] {
            "bg_empty", "common_grade", "junk_grade",
            "uncommon_grade", "rare_grade", "epic_grade", "legendary_grade", "item_selected"
        };

        for(String icon : icons) {
            skin.add("icon-"+icon, new TextureRegionDrawable(atlas.createSprite("icons/base/"+icon)), Drawable.class);
        }

        skin.add("icon-item_frame", ninePatchDrawable("icons/base/item_frame"), Drawable.class);
        skin.add("icon-equipment_item_frame", ninePatchDrawable("icons/base/equipment_item_frame"), Drawable.class);
//        skin.add("icon-junk_grade", new BaseDrawable(), Drawable.class);

        skin.add("confirm-overlay-rectangle", ninePatchDrawable("GUI/ui2/confirm-overlay-rectangle"), Drawable.class);

        skin.add("inventory-background", ninePatchDrawable("GUI/ui2/internal_panel4_background"), Drawable.class);

        ImageTextButton.ImageTextButtonStyle mergeAllButtonStyle = new ImageTextButton.ImageTextButtonStyle(ui2Button2NoPaddingStyle);
        mergeAllButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/mergeall1"));
        mergeAllButtonStyle.checked = null;
        mergeAllButtonStyle.checkedOver = null;
        skin.add("btn-merge-all", mergeAllButtonStyle);

        ImageTextButton.ImageTextButtonStyle splitStackButtonStyle = new ImageTextButton.ImageTextButtonStyle(ui2Button2NoPaddingStyle);
        splitStackButtonStyle.imageUp = new TextureRegionDrawable(atlas.createSprite("GUI/ui2/splitstack1"));
        splitStackButtonStyle.checked = null;
        splitStackButtonStyle.checkedOver = null;
        skin.add("btn-split-stack", splitStackButtonStyle);

        NinePatchDrawable dropDownNormal      = ninePatchDrawable("GUI/ui2/dropdown1_normal");
        NinePatchDrawable dropDownHovered     = ninePatchDrawable("GUI/ui2/dropdown1_hovered");
        NinePatchDrawable dropDownPressed     = ninePatchDrawable("GUI/ui2/dropdown1_pressed");
        NinePatchDrawable dropDownListBacking = ninePatchDrawable("GUI/ui2/dropdown1_listbacking1");

        NinePatchDrawable listBackground = ninePatchDrawable("GUI/ui2/internal_panel1_background");

        List.ListStyle listStyle = new List.ListStyle(font, Color.WHITE, Color.LIGHT_GRAY, gcListItemChecked);
        listStyle.background = dropDownListBacking;
        skin.add(RESOURCE_DEFAULT, listStyle);
        SelectBox.SelectBoxStyle selectBoxStyle = new SelectBox.SelectBoxStyle(font, Color.WHITE, dropDownNormal, scrollPaneStyle, listStyle);
        selectBoxStyle.backgroundOpen = dropDownPressed;
        selectBoxStyle.backgroundOver = dropDownHovered;
        skin.add(RESOURCE_DEFAULT, selectBoxStyle);

        return skin;
    }

    protected BitmapFont createFont(String fontPath) {
        final BitmapFont font = new BitmapFont(Gdx.files.internal(fontPath));
        font.getData().markupEnabled = true;
        return font;
    }

    private void zeroPadding(Drawable drawable) {
        drawable.setLeftWidth(0);
        drawable.setRightWidth(0);
        drawable.setTopHeight(0);
        drawable.setBottomHeight(0);
    }

    private void setPadding(Drawable drawable, float leftPadding, float rightPadding, float topPadding, float bottomPadding) {
        drawable.setLeftWidth(leftPadding);
        drawable.setRightWidth(rightPadding);
        drawable.setTopHeight(topPadding);
        drawable.setBottomHeight(bottomPadding);
    }

    protected NinePatchDrawable ninePatchDrawable(NinePatch patch, boolean updateMinSize) {
        NinePatchDrawable drawable = new NinePatchDrawable(patch);
        if(updateMinSize) {
            setupMinSize(drawable);
        }
        return drawable;
    }

    protected NinePatchDrawable ninePatchDrawable(String fileRef, boolean updateMinSize) {
        final NinePatch patch = atlas.createPatch(fileRef);
        if(patch == null) throw new IllegalStateException("Unable to find file "+fileRef);
        return ninePatchDrawable(patch, updateMinSize);
    }

    protected NinePatchDrawable ninePatchDrawable(String fileRef) {
        return ninePatchDrawable(fileRef, true);
    }

    protected NinePatchDrawable ninePatchDrawable(NinePatch patch) {
        return ninePatchDrawable(patch, true);
    }

    private void setupMinSize(NinePatchDrawable drawable) {
        drawable.setMinWidth(drawable.getPatch().getLeftWidth() + drawable.getPatch().getRightWidth());
        drawable.setMinHeight(drawable.getPatch().getTopHeight() + drawable.getPatch().getBottomHeight());
    }

    private void createSwitchStyle(Skin skin, BitmapFont font) {
        TextureRegion onTexture = atlas.createSprite("GUI/ui2/switch1-on-normal");
        TextureRegion offTexture = atlas.createSprite("GUI/ui2/switch1-off-normal");
        TextureRegion onOverTexture = atlas.createSprite("GUI/ui2/switch1-on-hovered");
        TextureRegion offOverTexture = atlas.createSprite("GUI/ui2/switch1-off-hovered");

        ImageTextButton.ImageTextButtonStyle switchStyle = new ImageTextButton.ImageTextButtonStyle();
        switchStyle.imageChecked = new TextureRegionDrawable(onTexture);
        switchStyle.imageUp = new TextureRegionDrawable(offTexture);
        switchStyle.imageOver = new TextureRegionDrawable(offOverTexture);
        switchStyle.imageCheckedOver = new TextureRegionDrawable(onOverTexture);
        switchStyle.font = font;
        skin.add("switch", switchStyle);

        TextureRegion checkOffTexture = atlas.createSprite("GUI/ui2/checkbox1-unchecked");
        TextureRegion checkOnTexture = atlas.createSprite("GUI/ui2/checkbox1-checked");

        ImageTextButton.ImageTextButtonStyle checkBoxStyle = new ImageTextButton.ImageTextButtonStyle();
        checkBoxStyle.imageChecked = new TextureRegionDrawable(checkOnTexture);
        checkBoxStyle.imageUp = new TextureRegionDrawable(checkOffTexture);
        checkBoxStyle.font = font;
        skin.add("default-checkable", checkBoxStyle, ImageTextButton.ImageTextButtonStyle.class);
        skin.add("default-checkable", checkBoxStyle, Button.ButtonStyle.class);
    }

    public static <T extends TransformDrawable> ImageTextButton.ImageTextButtonStyle createButtonStyle(T baseDrawable, BitmapFont font) {
        return createButtonStyle(baseDrawable, font, 1.1f, 1f);
    }

    public static <T extends TransformDrawable> ImageTextButton.ImageTextButtonStyle createButtonStyle(T baseDrawable, BitmapFont font, float overScale, float pressedOffset) {
        Drawable scaledDrawable = scaled(baseDrawable, overScale);
        ImageTextButton.ImageTextButtonStyle style = new ImageTextButton.ImageTextButtonStyle(baseDrawable, baseDrawable, baseDrawable, font);
        style.over = scaledDrawable;
        style.checkedOver = scaledDrawable;
        style.pressedOffsetX = pressedOffset;
        style.pressedOffsetY = pressedOffset;
        return style;
    }

    public static ImageTextButton.ImageTextButtonStyle createButtonStyle(TextureRegion textureRegion, BitmapFont font) {
        return createButtonStyle(textureRegion, font, 1.1f, 1f);
    }

    public static ImageTextButton.ImageTextButtonStyle createButtonStyle(TextureRegion textureRegion, BitmapFont font, float overScale, float pressedOffset) {
        return createButtonStyle(new TextureRegionDrawable(textureRegion), font, overScale, pressedOffset);
    }

    public static <T extends TransformDrawable> ImageButton.ImageButtonStyle createImageButtonStyle(T baseDrawable, float overScale, float pressedOffset) {
        Drawable overDrawable = scaled(baseDrawable, overScale);

        ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle(null, null, null, baseDrawable, baseDrawable, baseDrawable);
        style.imageOver = overDrawable;
        style.imageCheckedOver = overDrawable;
        style.pressedOffsetX = pressedOffset;
        style.pressedOffsetY = pressedOffset;
        return style;
    }

    protected static <T extends TransformDrawable> Drawable scaled(T baseDrawable, float overScale) {
        Drawable overDrawable;
        if (overScale == 1f) {
            overDrawable = baseDrawable;
        } else {
            ScaledDrawable<T> scaledDrawable = new ScaledDrawable<>(baseDrawable);
            scaledDrawable.setScale(overScale);
            overDrawable = scaledDrawable;
        }
        return overDrawable;
    }

    public static class ClippingTextureRegionDrawable extends TextureRegionDrawable {

        private final boolean flip;

        public ClippingTextureRegionDrawable(TextureRegion region, boolean flip) {
            super(region);
            this.flip = flip;
        }

        @Override
        public void draw(Batch batch, float x, float y, float width, float height) {
            int diff = (int) (getRegion().getRegionWidth() - width);
            if (diff < 0) diff = 0;
            if (flip) {
                x += diff;
            }
            batch.draw(getRegion().getTexture(), x, y,
                width, height, getRegion().getRegionX(), getRegion().getRegionY(), getRegion().getRegionWidth() - diff, getRegion().getRegionHeight(), flip, false);
        }
    }
}
