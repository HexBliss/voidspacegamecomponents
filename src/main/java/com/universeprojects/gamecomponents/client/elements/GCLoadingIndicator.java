package com.universeprojects.gamecomponents.client.elements;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.html5engine.client.framework.H5EConstantMotionBehaviour;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESprite;

public class GCLoadingIndicator extends H5ESprite {
    H5EConstantMotionBehaviour animation;

    public GCLoadingIndicator(H5ELayer layer) {
        super(layer, "images\\GUI\\ui2\\loading1.png");
        this.setVisible(false);
        this.setOrigin(Align.center);
    }

    public void activate(Group componentToOverlay) {
        if (animation==null)
            animation = new H5EConstantMotionBehaviour(this);
        this.setRotation(0);
        animation.setRotationSpeed(-6f);
        animation.activate();

        this.setVisible(true);
        componentToOverlay.addActor(this);

        float x = (componentToOverlay.getWidth()/2) - this.getWidth()/2;
        float y = (componentToOverlay.getHeight()/2) - this.getHeight()/2;

        this.setPosition(x, y);
    }

    public void deactivate()
    {
        if(animation != null) {
            removeBehaviour(animation);
            animation.deactivate();
            animation = null;
        }
        this.setVisible(false);
    }
}
