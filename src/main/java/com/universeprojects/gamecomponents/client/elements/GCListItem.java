package com.universeprojects.gamecomponents.client.elements;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

public abstract class GCListItem extends Button implements GraphicElement {

    private final GCListItemActionHandler actionHandler;

    private Integer level;

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public int getLevel() {
        if (level == null) return getLayer().getDefaultLevel();
        return level;
    }

    protected GCListItem(H5ELayer layer, GCListItemActionHandler<?> actionHandlerIn) {
        this(layer, actionHandlerIn, "gc-list-item");
    }

    protected GCListItem(H5ELayer layer, GCListItemActionHandler<?> actionHandlerIn, String style) {
        super(layer.getEngine().getSkin(), style);
        layer.addElement(this, null);
        setStage(layer);

        left().top();
        defaults().left().top();

        this.actionHandler = actionHandlerIn;
        if (actionHandler != null) {
            setTouchable(Touchable.enabled);
            addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    if (isChecked()) {
                        //noinspection unchecked
                        actionHandler.setSelection(GCListItem.this);
                    } else {
                        actionHandler.clearSelection();
                    }
                    event.handle();
                }
            });
            addListener(new ActorGestureListener() {
                @Override
                public void tap(InputEvent event, float x, float y, int count, int button) {
                    if(count == 2) {
                        //noinspection unchecked
                        actionHandler.onDoubleClick(GCListItem.this);
                    }
                }
            });
        }
    }

    /**
     * For internal use only.
     * Destroys the graphical representation of this item, such that it is not longer rendered by the engine.
     * This is intended to be done only when the item is no longer needed.
     */
    void destroy() {
        remove();
    }
}
