package com.universeprojects.gamecomponents.client.elements;

import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

public class GCSwitch extends H5EButton {

    public GCSwitch(H5ELayer layer) {
        super(layer, layer.getEngine().getSkin().get("switch", ImageTextButtonStyle.class));
    }

}
