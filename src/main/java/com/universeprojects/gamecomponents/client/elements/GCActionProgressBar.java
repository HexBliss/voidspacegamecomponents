package com.universeprojects.gamecomponents.client.elements;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.common.UIFactory;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EProgressBar;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EProgressBar.DisplayMode;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

public abstract class GCActionProgressBar extends Window implements GraphicElement {

    private final H5EProgressBar progressBar;
    private final H5EScrollablePane subtitleBar;
    private final H5ELabel subtitleText;
    private final Logger log = Logger.getLogger(GCActionProgressBar.class);

    private final static float PROGRESS_BAR_WIDTH = 320;

    private boolean maximumReached;
    private Integer level;

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public int getLevel() {
        if (level == null) return getLayer().getDefaultLevel();
        return level;
    }

    public GCActionProgressBar(H5ELayer layer, String leftButtonText, String rightButtonText) {
        this(layer, null, leftButtonText, rightButtonText);
    }

    public GCActionProgressBar(H5ELayer layer, Integer level, String leftButtonText, String rightButtonText) {
        super("", layer.getEngine().getSkin(), "action-progress-bar-window");
        layer.addElement(this, level);
        layer.addToTop(this);

        getTitleLabel().setAlignment(Align.center);
        getTitleLabel().setEllipsis(false);
        getTitleTable().padBottom(-80);

        setWidth(300);
        setHeight(300);
        padTop(100);
        padBottom(100);

        setClip(false);
        setTransform(true);

        setKeepWithinStage(false);

        subtitleBar = add(new H5EScrollablePane(layer))
                .colspan(2)
                .width(PROGRESS_BAR_WIDTH)
                .height(22)
                .getActor();
        subtitleBar.pack();
        subtitleBar.setOrigin(Align.center);
        subtitleText = subtitleBar.add(new H5ELabel(getLayer())).getActor();
        subtitleText.setFontScale(0.8f);
        subtitleText.setAlignment(Align.center);
        subtitleText.setWrap(false);
        row().padTop(10);

        progressBar = new H5EProgressBar(layer);
        final Cell<H5EProgressBar> progressBarCell = add(progressBar);

        progressBarCell.colspan(2);
        progressBarCell.width(PROGRESS_BAR_WIDTH);
        progressBarCell.height(20);
        progressBar.setOrigin(Align.center);
        progressBar.setDisplayMode(DisplayMode.LABEL_PERCENTAGE);
        row();

        H5EButton leftButton;
        if (Strings.isEmpty(leftButtonText)) {
            leftButton = null;
            add();
        } else {
            leftButton = new H5EButton(leftButtonText, layer);
            leftButton.getLabel().setColor(Color.valueOf("#FFFFFF"));
            leftButton.getLabel().setFontScale(1);
            leftButton.setOrigin(Align.left);
//            leftButton.setX(0, Align.center);
//            leftButton.setY(45, Align.center);
            leftButton.addButtonListener(this::onLeftBtnPressed);
            add(leftButton).width(PROGRESS_BAR_WIDTH / 2);
        }

        H5EButton rightButton;
        if (Strings.isEmpty(rightButtonText)) {
            rightButton = null;
            add();
        } else {
            rightButton = new H5EButton(rightButtonText, layer);
            rightButton.getLabel().setColor(Color.valueOf("#FFFFFF"));
            rightButton.getLabel().setFontScale(1);
            rightButton.setOrigin(Align.right);
//            rightButton.setX(0, Align.center);
//            rightButton.setY(45, Align.center);
            rightButton.addButtonListener(this::onRightBtnPressed);
            add(rightButton).width(PROGRESS_BAR_WIDTH / 2);
        }

        maximumReached = false;

        // Start off hidden
        hide();
    }

    public void show() {
        setVisible(true);
        toFront();
    }

    public void hide() {
        setVisible(false);
    }

    public void dismiss() {
        clear();
        hide();
    }

    public void positionProportionally(float propX, float propY) {
        setX(getEngine().getWidth() * propX - getOriginX() - getWidth() / 2f);
        setY(getEngine().getHeight() * propY - getOriginY() - getHeight() / 2f);
    }

    public void setCaption(String text) {
        getTitleLabel().setText(text);
    }

    public void setSubtitle(String subtitle) {
        subtitleText.setText(subtitle);
    }

    public void setMaximum(int maximum) {
        progressBar.setRange(0, maximum);
    }

    public float getMaximimum() {
        return progressBar.getMaxValue();
    }

    public void setProgress(int progress) {
        progressBar.setValue(progress);
        if (!maximumReached && progress >= progressBar.getMaxValue()) {
            maximumReached = true;
            onMaximumReached();
        }
    }

    @Override
    public void clear() {
        setCaption("");
        subtitleText.setText("");
        setProgress(0);
        setMaximum(1000);
        maximumReached = false;
    }

    public abstract void onLeftBtnPressed();

    public abstract void onRightBtnPressed();

    public abstract void onMaximumReached();

}
