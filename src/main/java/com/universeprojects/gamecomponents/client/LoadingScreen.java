package com.universeprojects.gamecomponents.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Disposable;

public class LoadingScreen implements Disposable {

    private SpriteBatch spriteBatch;
    private float time = 0f;
    private AssetManager am;
    private Texture loadingBarBackground;
    private Texture loadingBar;
    private Texture loginBackground;

    private int initialX;
    private int initialY;
    private NinePatchDrawable drawable;
    private final float barWidthLimit;
    public LoadingScreen(AssetManager am2) {
        this.am=am2;
       // am=new AssetManager();
        loadingBarBackground = new Texture(Gdx.files.internal("images/GUI/ui2/preload/loading-bar-background.png"));
        loadingBar = new Texture(Gdx.files.internal("images/GUI/ui2/preload/loading-bar.png"));
        loginBackground = new Texture(Gdx.files.internal("images/GUI/ui2/preload/startup-background1.png"));



        NinePatch patch = new NinePatch(loadingBar, 12, 12, 12, 12);
        drawable= new NinePatchDrawable(patch);

        spriteBatch=new SpriteBatch();

        initialX=Gdx.graphics.getWidth() / 2 - loadingBarBackground.getWidth() / 2;
        initialY=Gdx.graphics.getHeight() / 2 - loadingBarBackground.getHeight() / 2;

        barWidthLimit=loadingBar.getWidth()+21;

        /*
        load();

        initialX=Gdx.graphics.getWidth() / 2 - loadingBarBackground.getWidth() / 2;
        initialY=Gdx.graphics.getHeight() / 2 - loadingBarBackground.getHeight() / 2;

        */
    }

    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); // Clear screen
        Gdx.gl.glClearColor(0.8f, 0.8f, 0.8f, 1f);
        time += Gdx.graphics.getDeltaTime(); // Accumulate elapsed animation time


        spriteBatch.begin();

        spriteBatch.draw(loginBackground,0,0);
        spriteBatch.draw(loadingBarBackground, initialX, initialY,loadingBarBackground.getWidth(),loadingBarBackground.getHeight());

        //float currentWidth= loadingBar.getWidth() * time;
        float currentWidth=calculateBarWidth(time);
        //float currentWidth= loadingBar.getWidth() * am.getProgress();
        /*
        if(currentWidth>loadingBar.getWidth()+21){
            currentWidth=loadingBar.getWidth()+21;
        }
        */

        /*
        spriteBatch.draw(loadingBar, initialX+(20/2), initialY+7,
                currentWidth/2, loadingBar.getHeight()+8);
                */
        spriteBatch.draw(loadingBar, initialX+20, initialY+7,
                currentWidth, loadingBar.getHeight()+8);
        spriteBatch.end();

    }
    public float calculateBarWidth(float elapsedTime){
        float barWidth;
        //barWidth= ( (am.getProgress()*300)+(elapsedTime*20) );
        barWidth=((loadingBar.getWidth() * elapsedTime)/2f )+ (am.getProgress()*1500);

        //barWidth=((600 * elapsedTime)/1.5f )+ (am.getProgress()*1000);
        /*
        float progress=am.getProgress()*1000;
        System.err.println("progress: "+progress);
        System.err.println("time contribution: "+((loadingBar.getWidth() * elapsedTime)/1.5f ));
        */
        /*
        if(barWidth>barWidthLimit){
            barWidth=barWidthLimit;
        }
        */
        if(barWidth>905){
            barWidth=905;
        }

        return barWidth;
    }

    @Override
    public void dispose() {
        spriteBatch.dispose();
        loadingBar.dispose();
        loadingBarBackground.dispose();
        loginBackground.dispose();
    }
}
