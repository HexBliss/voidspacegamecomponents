package com.universeprojects.gamecomponents.client.dialogs.invention;

import java.util.ArrayList;
import java.util.List;

public class GCKnowledgeData {

    private final List<GCKnowledgeDataItem> items = new ArrayList<>();

    private GCKnowledgeData() {
    }

    public static GCKnowledgeData createNew() {
        return new GCKnowledgeData();
    }

    public GCKnowledgeData add(GCKnowledgeData other) {
        for (GCKnowledgeDataItem item : other.items) {
            // we call the add() method sequentially in order for new indices to be assigned
            add(item.name, item.exp);
        }
        return this;
    }

    public GCKnowledgeData add(String name, int exp) {
        int index = items.size();
        items.add(new GCKnowledgeDataItem(index, name, exp));
        return this;
    }

    public GCKnowledgeDataItem get(int index) {
        return items.get(index);
    }

    public int size() {
        return items.size();
    }

    public static class GCKnowledgeDataItem {
        final int index;
        final String name;
        final int exp;

        GCKnowledgeDataItem(int index, String name, int exp) {
            this.index = index;
            this.name = name;
            this.exp = exp;
        }
    }

}


