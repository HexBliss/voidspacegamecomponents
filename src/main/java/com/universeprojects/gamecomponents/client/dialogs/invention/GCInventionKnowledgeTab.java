package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.universeprojects.gamecomponents.client.dialogs.invention.GCKnowledgeData.GCKnowledgeDataItem;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;

class GCInventionKnowledgeTab extends GCInventionTab {

    private final H5EScrollablePane listContainer;
    private GCList<GCKnowledgeListItem> knowledgeList;
    private final GCListItemActionHandler<GCKnowledgeListItem> listHandler;
    public static final int LIST_SPACING = 5;

    GCInventionKnowledgeTab(GCInventionSystemDialog dialog) {
        super(dialog, "Knowledge");

        listContainer = rightContent.add(new H5EScrollablePane(layer)).grow().getActor();
        listContainer.getContent().top().left();

        listHandler = new GCListItemActionHandler<GCKnowledgeListItem>() {
            @Override
            protected void onSelectionUpdate(GCKnowledgeListItem lastSelectedItem) {
                // do nothing
            }
        };
    }

    @Override
    protected void clearTab() {
        if (knowledgeList != null) {
            listHandler.reset();
            knowledgeList.clear();
            knowledgeList.remove();
            knowledgeList = null;
            listContainer.getContent().clear();
        }
    }

    void populateData(GCKnowledgeData knowledgeData) {
        clearTab();

        knowledgeList = new GCList<>(layer, 1, LIST_SPACING, LIST_SPACING, true);
        knowledgeList.left().top();
        listContainer.add(knowledgeList).left().top().grow();
        for (int index = 0; index < knowledgeData.size(); index++) {
            GCKnowledgeDataItem dataItem = knowledgeData.get(index);
            GCKnowledgeListItem listItem = new GCKnowledgeListItem(layer, listHandler, dataItem);
            knowledgeList.addItem(listItem);
        }
    }

}
