package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.universeprojects.common.shared.util.Log;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData.GCInventoryDataItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class GCRecipeData {

    public enum RecipeMode {
        ITEM_IDEA, STRUCTURE_IDEA, ITEM_SKILL, STRUCTURE_SKILL
    }

    private final RecipeMode mode;
    private final Long referenceId;
    private final List<GCRecipeSlotDataItem> slots = new ArrayList<>();

    private GCRecipeData(RecipeMode mode, Long referenceId) {
        if (mode == null) {
            throw new IllegalArgumentException("Mode can't be null");
        }
        if (referenceId == null) {
            throw new IllegalArgumentException("Reference id can't be null");
        }

        this.mode = mode;
        this.referenceId = referenceId;
    }

    public RecipeMode getMode() {
        return mode;
    }

    public Long getReferenceId() {
        return referenceId;
    }

    public static GCRecipeData createNew(RecipeMode mode, Long referenceId) {
        return new GCRecipeData(mode, referenceId);
    }

    public GCRecipeData copy() {
        GCRecipeData copy = new GCRecipeData(mode, referenceId);
        slots.forEach((slot) -> copy.slots.add(slot.copy()));
        return copy;
    }

    public GCRecipeData addSlot(String id, String name, Boolean required, String selectedItemUid, GCInventoryData items) {
        int index = slots.size();
        slots.add(new GCRecipeSlotDataItem(index, id, name, required, selectedItemUid, items));
        return this;
    }

    public GCRecipeSlotDataItem getSlot(int index) {
        return slots.get(index);
    }

    public List<GCRecipeSlotDataItem> getSlots() {
        return Collections.unmodifiableList(slots);
    }

    public int size() {
        return slots.size();
    }

    public static class GCRecipeSlotDataItem {
        public final int index;

        public final String id;
        public final String name;
        /**
         * When this field is null, it means that this is not specified
         */
        public final Boolean required;

        private GCInventoryData items;
        private String selectedItemUid;

        private GCRecipeSlotDataItem(GCRecipeSlotDataItem copyFrom) {
            this.index = copyFrom.index;

            this.id = copyFrom.id;
            this.name = copyFrom.name;
            this.required = copyFrom.required;
            this.items = copyFrom.items.copy();

            this.selectedItemUid = copyFrom.selectedItemUid;
        }

        GCRecipeSlotDataItem(int index, String id, String name, Boolean required, String selectedItemUid, GCInventoryData items) {
            this.index = index;

            this.id = id;
            this.name = name;
            this.required = required;
            this.items = items;

            this.selectedItemUid = selectedItemUid;
        }

        public GCRecipeSlotDataItem copy() {
            return new GCRecipeSlotDataItem(this);
        }

        public void clearSelectedItem() {
            selectedItemUid = null;
        }

        public void selectItem(String uid) {
            if (!items.hasUid(uid)) {
                throw new IllegalStateException("uid not found: " + uid);
            }
            Log.info("Selected item uid: " + uid);
            selectedItemUid = uid;
        }

        public boolean isItemSelected() {
            return selectedItemUid != null;
        }

        public GCInventoryDataItem getSelectedItem() {
            if (selectedItemUid == null) {
                return null;
            } else {
                return items.getByUid(selectedItemUid);
            }
        }

        public GCInventoryData getItems() {
            return this.items.copy();
        }

        public void replaceItems(GCInventoryData items) {
            Set<String> departingUids = this.items.getUids();
            departingUids.removeAll(items.getUids());

            if (departingUids.contains(selectedItemUid)) {
                selectedItemUid = null;
            }

            this.items = items;
        }

    }

}


