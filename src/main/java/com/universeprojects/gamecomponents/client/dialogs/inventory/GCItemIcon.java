package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

public class GCItemIcon<T extends GCInventoryItem> extends WidgetGroup implements GraphicElement {
    public static final int DEFAULT_ICON_SIZE = 64;

    public static final int BASE_ICON_SIZE = 50;
    public static final int BASE_EMPTY_ICON_SIZE = 42;
    public static final int BASE_EMPTY_FRAME_ICON_SIZE = 62;
    public static final int BASE_FRAME_ICON_SIZE = 46;
    public static final int BASE_SELECTED_ICON_SIZE = 52;
    public static final int BASE_ITEM_ICON_SIZE = 38;
    public static final int BASE_LABEL_OFFSET_X = 8;
    public static final int BASE_LABEL_OFFSET_Y = 4;
    private final SlotConfig slotConfig;

    private H5ELabel quantityLabel;
    private H5EIcon rarityIcon;
    private H5ELabel emptyNameLabel;
    private H5EIcon emptyFrameIcon;
    private H5EIcon emptyIcon;
    private H5EIcon itemIcon;
    private H5EIcon frameIcon;
    private H5EIcon selectedIcon;
    private H5EIcon highlightIcon;


    private Integer level;

    private T item;
    private int prefSize;
    private boolean updatePrefSizeFromIcon = true;

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public int getLevel() {
        if (level == null) return getLayer().getDefaultLevel();
        return level;
    }

    public GCItemIcon(H5ELayer layer) {
        this(layer, null);
    }

    public GCItemIcon(H5ELayer layer, SlotConfig slotConfig) {
        this.slotConfig = slotConfig;
        layer.addElement(this, null);
        setStage(layer);
        setItem(null);
        prefSize = calculatePrefSize(DEFAULT_ICON_SIZE);
    }

    @Override
    public float getPrefWidth() {
        return prefSize;
    }

    @Override
    public float getPrefHeight() {
        return prefSize;
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
        if(item != null) {
            final String rarityIconStyle = item.getRarityIconStyle();
            if(rarityIcon == null) {
                rarityIcon = H5EIcon.fromStyle(getLayer(), rarityIconStyle);
                addActor(rarityIcon);
            } else {
                rarityIcon.setDrawable(getEngine().getSkin(), rarityIconStyle);
            }

            if(frameIcon == null) {
                frameIcon = H5EIcon.fromStyle(getLayer(), getFrameStyle());
                addActor(frameIcon);
            }

            if(itemIcon == null) {
                itemIcon = H5EIcon.fromSpriteType(getLayer(), item.getIconSpriteKey(), Scaling.stretch, Align.center);
                addActor(itemIcon);
            } else {
                H5ESpriteType spriteType = (H5ESpriteType) getEngine().getResourceManager().getSpriteType(item.getIconSpriteKey());
                itemIcon.setDrawable(new TextureRegionDrawable(spriteType.getGraphicData()));
            }

            if(item.isStackable()) {
                if (quantityLabel == null) {
                    quantityLabel = new H5ELabel(getLayer());
                    quantityLabel.setStyle(getLayer().getEngine().getSkin().get("label-small", Label.LabelStyle.class));
                    addActor(quantityLabel);
                }
                quantityLabel.setVisible(true);
                quantityLabel.setText(String.valueOf(item.getQuantity()));
            } else if(quantityLabel != null) {
                quantityLabel.setVisible(false);
            }

            setIconVisible(true, rarityIcon, frameIcon, itemIcon);
            setIconVisible(false, emptyIcon, emptyNameLabel, emptyFrameIcon);

            if(updatePrefSizeFromIcon) {
                prefSize = calculatePrefSize(Math.max(itemIcon.getPrefWidth(), itemIcon.getPrefHeight()));
            }

        } else {
            if(emptyIcon == null) {
                String spriteTypeKey;
                if(slotConfig != null && slotConfig.getEmptySlotIcon() != null) {
                    spriteTypeKey = slotConfig.getEmptySlotIcon();
                } else {
                    spriteTypeKey = getEmptySpriteKey();
                }
                emptyIcon = H5EIcon.fromSpriteType(getLayer(), spriteTypeKey);
                addActorAt(0, emptyIcon);
            }
            if(emptyFrameIcon == null) {
                String style = getEmptyFrameIconStyle();
                if(style != null) {
                    emptyFrameIcon = H5EIcon.fromStyle(getLayer(), style);
                    addActor(emptyFrameIcon);
                }
            }
            if(emptyNameLabel == null && slotConfig != null && slotConfig.getName() != null) {
                emptyNameLabel = new H5ELabel(getLayer());
                emptyNameLabel.setStyle(getLayer().getEngine().getSkin().get("label-small", Label.LabelStyle.class));
                emptyNameLabel.setText(slotConfig.getName());
                emptyNameLabel.setColor(Color.LIGHT_GRAY);
                emptyNameLabel.setAlignment(Align.center);
                emptyNameLabel.setWrap(true);
                addActor(emptyNameLabel);
            }
            setIconVisible(false, rarityIcon, frameIcon, itemIcon, quantityLabel);
            setIconVisible(true, emptyIcon, emptyNameLabel, emptyFrameIcon);

            if(updatePrefSizeFromIcon) {
                prefSize = calculatePrefSize(DEFAULT_ICON_SIZE);
            }
        }
    }

    @Override
    public void setColor(Color color) {
        super.setColor(color);
        if(emptyIcon != null) {
            emptyIcon.setColor(color);
        }
        if(itemIcon != null) {
            itemIcon.setColor(color);
        }
        if(frameIcon != null) {
            frameIcon.setColor(color);
        }
        if(rarityIcon != null) {
            rarityIcon.setColor(color);
        }
    }

    public void layout() {
        float iconSize = getWidth();
        float factor = iconSize / (float)BASE_ICON_SIZE;
        float emptyIconSize = factor * BASE_EMPTY_ICON_SIZE;
        float emptyFrameIconSize = factor * BASE_EMPTY_FRAME_ICON_SIZE;
        float frameIconSize = factor * getFrameBaseIconSize();
        float itemIconSize = factor * BASE_ITEM_ICON_SIZE;
        float selectedIconSize = factor * BASE_SELECTED_ICON_SIZE;
        float labelOffsetX = factor * BASE_LABEL_OFFSET_X;
        float labelOffsetY = factor * BASE_LABEL_OFFSET_Y;
        if(rarityIcon != null) {
            rarityIcon.setSize(iconSize, iconSize);
            rarityIcon.validate();
        }
        if(emptyIcon != null) {
            emptyIcon.setSize(emptyIconSize, emptyIconSize);
            final float emptyOffset = ((iconSize - emptyIconSize) / 2);
            emptyIcon.setPosition(emptyOffset, emptyOffset);
            emptyIcon.validate();
        }
        if(emptyFrameIcon != null) {
            emptyFrameIcon.setSize(emptyFrameIconSize, emptyFrameIconSize);
            final float frameOffset = ((iconSize - emptyFrameIconSize) / 2);
            emptyFrameIcon.setPosition(frameOffset, frameOffset);
            emptyFrameIcon.validate();
        }
        if(emptyNameLabel != null) {
            emptyNameLabel.setSize(iconSize, emptyNameLabel.getPrefHeight());
            emptyNameLabel.setPosition(0, (iconSize-emptyNameLabel.getPrefHeight())/2);
            emptyNameLabel.validate();
        }
        if(frameIcon != null) {
            frameIcon.setSize(frameIconSize, frameIconSize);
            final float frameOffset = ((iconSize - frameIconSize) / 2);
            frameIcon.setPosition(frameOffset, frameOffset);
            frameIcon.validate();
        }
        if(itemIcon != null) {
            itemIcon.setSize(itemIconSize, itemIconSize);
            final float itemOffset = ((iconSize - itemIconSize) / 2);
            itemIcon.setPosition(itemOffset, itemOffset);
            itemIcon.validate();
        }
        if(quantityLabel != null) {
            final float offset = (iconSize - labelOffsetX);
            quantityLabel.setSize(quantityLabel.getPrefWidth(), quantityLabel.getPrefHeight());
            quantityLabel.setPosition(offset, labelOffsetY, Align.bottomRight);
            quantityLabel.validate();
        }
        if(isSelected()) {
            if(selectedIcon == null) {
                selectedIcon = H5EIcon.fromStyle(getLayer(), getSelectedStyle());
                addActor(selectedIcon);
            }
            selectedIcon.setSize(selectedIconSize, selectedIconSize);
            selectedIcon.setVisible(true);
        } else if(selectedIcon != null) {
            selectedIcon.setVisible(false);
        }

        if(isHighlighted()) {
            if(highlightIcon == null) {
                highlightIcon = H5EIcon.fromStyle(getLayer(), getHighlightStyle());
                addActor(emptyIcon);
            }
            highlightIcon.setSize(selectedIconSize, selectedIconSize);
            highlightIcon.setVisible(true);
        } else if(highlightIcon != null) {
            highlightIcon.setVisible(false);
        }
    }

    protected int calculatePrefSize(float iconSize) {
        float factor = iconSize / (float)BASE_ITEM_ICON_SIZE;
        return (int) (factor*BASE_ICON_SIZE);
    }

    protected boolean isSelected() {
        return false;
    }

    protected boolean isHighlighted() {
        return false;
    }

    protected String getEmptySpriteKey() {
        return "images/icons/base/bg_empty.png";
    }

    protected String getFrameStyle() {
        return "icon-item_frame";
    }

    protected String getEmptyFrameIconStyle() {
        return "icon-item_frame";
    }

    protected String getSelectedStyle() {
        return "icon-item_selected";
    }

    protected String getHighlightStyle() {
        //TODO
        throw new IllegalStateException("Needs graphics");
    }

    protected int getFrameBaseIconSize() {
        return BASE_FRAME_ICON_SIZE;
    }

    public void setManualIconSize(float size) {
        prefSize = calculatePrefSize(size);
        updatePrefSizeFromIcon = false;
    }

    private void setIconVisible(boolean value, Actor... icons) {
        for(Actor icon : icons) {
            if(icon != null) {
                icon.setVisible(value);
            }
        }
    }
}
