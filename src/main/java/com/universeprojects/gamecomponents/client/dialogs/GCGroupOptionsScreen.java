package com.universeprojects.gamecomponents.client.dialogs;

import com.universeprojects.gamecomponents.client.elements.GCErrorMessage;
import com.universeprojects.gamecomponents.client.windows.Alignment;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gamecomponents.client.windows.Position;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;

@SuppressWarnings({"FieldCanBeLocal", "unused", "ConstantConditions", "StatementWithEmptyBody"})
public abstract class GCGroupOptionsScreen {

    private final H5ELayer layer;
    private final GCGroupViewer groupViewer;

    private final GCSimpleWindow window;
    private final H5EInputBox groupDescriptionInput;
    private final GCErrorMessage errorMessage;
    private final H5EButton btnUpdate;
    private final H5EButton btnCloseGroup;

    private String groupName;

    private final static int DESCRIPTION_MAX_LENGTH = 500;

    public GCGroupOptionsScreen(H5ELayer layer, GCGroupViewer groupViewer) {
        this.layer = layer;
        this.groupViewer = groupViewer;

        window = new GCSimpleWindow(layer, "group-options", "Group Options", 475, 200, true);
        window.positionProportionally(.7f, .7f);
        window.onClose.registerHandler(this::clearContent);

        window.addH2("Group description:", Position.NEW_LINE, Alignment.LEFT, 0, 0);

        window.addEmptyLine(5);
        groupDescriptionInput = window.addInputBox(425, Position.NEW_LINE, Alignment.LEFT, 0, 0);
        // TODO: we need a text box with greater height!

        window.addEmptyLine(150);
        errorMessage = window.addElement(new GCErrorMessage(layer), Position.NEW_LINE, Alignment.CENTER, 0, 0);

        btnUpdate = window.addMediumButton("Update", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        btnUpdate.addButtonListener(this::handleBtnUpdate);

        btnCloseGroup = window.addMediumButton("Close Group", Position.SAME_LINE, Alignment.RIGHT, 0, 0);
        btnCloseGroup.addButtonListener(this::handleBtnCloseGroup);

        clearContent();
    }

    /**
     * Returns this viewer's content to the default state
     */
    private void clearContent() {
        groupName = null;
        errorMessage.clearError();
    }

    /**
     * Opens the window
     */
    public void open() {
        if (refreshContent()) {
            window.open();
        }
    }

    /**
     * Closes the window
     */
    public void close() {
        window.close();
    }

    /**
     * Toggles the window
     */
    public void toggle() {
        if (isOpen()) {
            close();
        } else {
            open();
        }
    }

    /**
     * @return TRUE if the window is open, FALSE otherwise
     */
    public boolean isOpen() {
        return window.isOpen();
    }

    private void handleBtnUpdate() {
        errorMessage.clearError();

        String newDescription = groupDescriptionInput.getText();
        if (newDescription.length() > DESCRIPTION_MAX_LENGTH) {
            errorMessage.error("Description must be at most " + DESCRIPTION_MAX_LENGTH + " letters long");
        } else {
            doUpdate(groupName, newDescription);
            window.close();
            groupViewer.close();
        }
    }

    private void handleBtnCloseGroup() {
        errorMessage.clearError();

        // Ask the user for a confirmation
        GCInfoDialog confirmation = new GCInfoDialog(layer, true) {

            @Override
            protected String getTitleText() {
                return "Confirmation";
            }

            @Override
            protected String[] getCaptionText() {
                return new String[]{
                    "Are you sure that you want to close the group?",
                    "All memberships will be lost. This action is irreversible!",
                };
            }

            @Override
            protected String getButtonLeftText() {
                return "CONFIRM";
            }

            @Override
            protected String getButtonRightText() {
                return "CANCEL";
            }

            @Override
            protected void onLeftButtonPressed() {
                confirm();
            }

            @Override
            protected void onRightButtonPressed() {
                cancel();
            }

            @Override
            public void onDismiss() {
                cancel(); // closing the window is equivalent to "Cancel"
            }

            private void confirm() {
                // action confirmed
                doCloseGroup(groupName);
                window.close();
                groupViewer.close();
            }

            private void cancel() {
                // changed their mind... phew! nothing needs to be done
            }

        };

        confirmation.positionProportionally(.8f, .2f);
        confirmation.open();
    }

    /**
     * Sets the content of this window, based on the current user's group
     *
     * @return TRUE if the content was set successfully, FALSE in case of an issue
     */
    protected abstract boolean refreshContent();

    protected abstract void doUpdate(String groupName, String desc);

    protected abstract void doCloseGroup(String groupName);

}
