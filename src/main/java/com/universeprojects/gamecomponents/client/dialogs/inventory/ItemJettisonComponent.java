package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;

/**
 * This class allows items to be jettisoned into space
 */
public class ItemJettisonComponent<K, T extends GCInventoryItem, C extends GCInventorySlotBaseComponent<K, T, C>> extends DragAndDrop.Target {
	public ItemJettisonComponent(Actor actor) {
		super(actor);
	}

	/**
	 * Determine whether an item can be jettisoned
	 * @param source  The inventory that the item is being dragged from
	 * @param payload The item that is being dragged
	 * @return Whether the item can be jettisoned
	 */
	@Override
	public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
		ItemPayload<?, ?, ?> itemPayload = (ItemPayload<?, ?, ?>) payload;
		ComponentSource<?, ?, ?> componentSource = (ComponentSource<?, ?, ?>) source;
		return canJettison(componentSource, itemPayload);
	}

	/**
	 * Jettison an item
	 * @param source  The inventory that the item is being dragged from
	 * @param payload The item that is being dragged
	 */
	@Override
	public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
		ItemPayload<?, ?, ?> itemPayload = (ItemPayload<?, ?, ?>) payload;
		ComponentSource<?, ?, ?> componentSource = (ComponentSource<?, ?, ?>) source;
		jettison(componentSource, itemPayload);
	}

	/**
	 * Jettison an item
	 * @param componentSourceUntyped  The inventory that the item is being dragged from
	 * @param itemPayloadUntyped      The item that is being dragged
	 */
	@SuppressWarnings({"unchecked", "ConstantConditions"})
	private <KT, IT extends GCInventoryItem, CT extends GCInventorySlotBaseComponent<KT, IT, CT>>
	void jettison(ComponentSource<?, ?, ?> componentSourceUntyped, ItemPayload<?, ?, ?> itemPayloadUntyped) {
		ComponentSource<KT, IT, CT> componentSource = (ComponentSource<KT, IT, CT>) componentSourceUntyped;
		ItemPayload<KT, IT, CT> itemPayload = (ItemPayload<KT, IT, CT>) itemPayloadUntyped;
		final GCInventoryBase<KT, IT, CT> sourceInventory = componentSource.getActor().inventoryBase;
		final KT sourceKey = componentSource.getActor().getKey();

		sourceInventory.jettison(sourceKey, itemPayload.getObject());
	}

	/**
	 * Determine whether an item can be jettisoned
	 * @param componentSourceUntyped  The inventory that the item is being dragged from
	 * @param itemPayloadUntyped      The item that is being dragged
	 * @return Whether the item can be jettisoned
	 */
	@SuppressWarnings({"unchecked", "ConstantConditions"})
	private <KT, IT extends GCInventoryItem, CT extends GCInventorySlotBaseComponent<KT, IT, CT>>
	boolean canJettison(ComponentSource<?, ?, ?> componentSourceUntyped, ItemPayload<?, ?, ?> itemPayloadUntyped) {
		ComponentSource<KT, IT, CT> componentSource = (ComponentSource<KT, IT, CT>) componentSourceUntyped;
		ItemPayload<KT, IT, CT> itemPayload = (ItemPayload<KT, IT, CT>) itemPayloadUntyped;
		final GCInventoryBase<KT, IT, CT> sourceInventory = componentSource.getActor().inventoryBase;

		return sourceInventory.canJettison(itemPayload.getObject());
	}
}
