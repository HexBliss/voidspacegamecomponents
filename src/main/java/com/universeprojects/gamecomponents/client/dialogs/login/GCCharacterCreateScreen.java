package com.universeprojects.gamecomponents.client.dialogs.login;


import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.UPUtils;

import java.util.ArrayList;
import java.util.List;


public class GCCharacterCreateScreen<L extends LoginInfo, S extends ServerInfo> {

    private final GCWindow window;

    private final H5EInputBox fieldCharacterName;
    private final H5ELabel characterWarningText;
    private final Color warningColor;
    private static final int LOGIN_FIELD_W = 350;
    public static final int ICON_SIZE = 64;
    final float warningTextFontSize = 0.75f;
    final float loginInfoFontSize = 1.1f;
    private final GCLoadingIndicator loadingIndicator;

    private final GCLoginManager<L, S, ?> loginManager;

    private final List<String> nameList = new ArrayList<>();

    private L loginInfo;
    private S serverInfo;

    public GCCharacterCreateScreen(H5ELayer layer, GCLoginManager<L, S, ?> loginManager) {
        this.loginManager = loginManager;

        loadingIndicator = new GCLoadingIndicator(layer);
        
        window = new GCWindow(layer, 444, 300, "login-window");
        window.setId("login-screen");
        window.setTitle("Create your character");
        window.positionProportionally(0.5f, 0.5f);
        window.setPackOnOpen(false);
        window.setMovable(false);
        window.defaults().center();
        window.setCloseButtonEnabled(false);
        warningColor = Color.RED;
        //Align title onto tab at the top
        window.padTop(40);
        window.getTitleTable().padRight(20);

        //Create login tab

        H5ELabel characterNameLabel = new H5ELabel(layer);
        characterNameLabel.setFontScale(loginInfoFontSize);
        characterNameLabel.setText("Character Name");

        //Space to enter email
        fieldCharacterName = new H5EInputBox(layer);
        fieldCharacterName.setWidth(LOGIN_FIELD_W);

        //Warning text for invalid emails
        characterWarningText = new H5ELabel(layer);
        characterWarningText.setFontScale(warningTextFontSize);
        characterWarningText.setColor(warningColor);
        characterWarningText.setAlignment(Align.center);
        characterWarningText.setVisible(false);

        H5EButton btnCharacterCreate = new H5EButton("Join with new Character", layer);
        btnCharacterCreate.addButtonListener(this::createCharacter);
        H5EButton btnBack = new H5EButton("Back", layer);
        btnBack.addButtonListener(this::back);


        window.add(characterNameLabel);
        window.row();
        window.add(fieldCharacterName).width(350);
        window.row();
        window.add(characterWarningText);
        window.row();
        window.add(btnCharacterCreate);
        window.row();
        window.add(btnBack).width(150).padTop(10);

        UPUtils.tieButtonToKey(fieldCharacterName, Input.Keys.ENTER, btnCharacterCreate);
    }

    private void back() {
        close();
        loginManager.openCharacterSelectScreen(loginInfo, serverInfo);
    }

    public void refreshUserCharacterList() {
        nameList.clear();
        loadingIndicator.activate(window);
        loginManager.loadUserCharacters(loginInfo, (list) -> {
            nameList.addAll(list);
            loadingIndicator.deactivate();
        }, (error) -> {
            loadingIndicator.deactivate();
            //TODO
        });
    }

    private void createCharacter() {
        if (isValidCharacterName()) {
            final String characterName = fieldCharacterName.getText();
            loadingIndicator.activate(window);
            loginManager.joinWithNewCharacter(loginInfo, serverInfo, characterName, this::close, (error) -> {
                loadingIndicator.deactivate();
                if("char_in_use_ingame".equals(error)) {
                    characterWarningText.setText("Character name is already in use in this game. \nPlease use a different name.");
                } else if("char_owned_by_other_user".equals(error)) {
                    characterWarningText.setText("Character name is already in use by another player. \nPlease use a different name.");
                } else {
                    characterWarningText.setText("An unexpected error happened while creating the character. \nPlease try again later.");
                }
                characterWarningText.setVisible(true);
                fieldCharacterName.setColor(warningColor);
            });
        }
    }

    private boolean isValidCharacterName() {
        String characterName = fieldCharacterName.getText();
        if (characterName.isEmpty()) {
            characterWarningText.setText("Please enter your character name.");
            characterWarningText.setVisible(true);
            fieldCharacterName.setColor(warningColor);
            return false;
        }
        //Temporary replacement for database search of character names in use.
        //Do Some cool database stuff to check if the charactername
        //is current in use. If it is, return false. mission failed we'll get em next time.
        if (characterName.equals("donte")) {
            characterWarningText.setText("Character name is already in use. ");
            characterWarningText.setVisible(true);
            fieldCharacterName.setColor(warningColor);
            return false;
        }
        return true;
    }

    public GCWindow getWindow() {
        return window;
    }

    public void open(L loginInfo, S serverInfo) {
        fieldCharacterName.setText("");
        this.loginInfo = loginInfo;
        this.serverInfo = serverInfo;
        window.open();
        refreshUserCharacterList();
    }


    public void close() {
        loadingIndicator.deactivate();
        window.close();
    }

    public boolean isOpen() {
        return window.isOpen();
    }

    public void positionProportionally(float propX, float propY) {
        window.positionProportionally(propX, propY);
    }
}

