package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;

class ComponentTarget<K, T extends GCInventoryItem, C extends GCInventorySlotBaseComponent<K, T, C>> extends DragAndDrop.Target {
    private final C component;

    public ComponentTarget(C component) {
        super(component);
        this.component = component;
    }

    @Override
    public C getActor() {
        return component;
    }

    @Override
    public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
        ItemPayload<?, ?, ?> itemPayload = (ItemPayload<?, ?, ?>) payload;
        ComponentSource<?, ?, ?> componentSource = (ComponentSource<?, ?, ?>) source;
        final GCInventoryBase<?, ?, ?> sourceInventory = componentSource.getActor().inventoryBase;
        final boolean canDrop;
        if (component.inventoryBase == sourceInventory) {
            //noinspection unchecked
            ItemPayload<K, T, C> localPayload = (ItemPayload<K, T, C>) itemPayload;
            canDrop = component.inventoryBase.canSameInventoryDrop(localPayload.getSourceComponent().getKey(), localPayload.getObject(), component.getKey());
        } else {
            canDrop = canCrossInventoryDrop(componentSource, itemPayload);
        }
        return canDrop;
    }

    @SuppressWarnings({"unchecked", "ConstantConditions"})
    private <KT, IT extends GCInventoryItem, CT extends GCInventorySlotBaseComponent<KT, IT, CT>>
    boolean canCrossInventoryDrop(ComponentSource<?, ?, ?> componentSourceUntyped, ItemPayload<?, ?, ?> itemPayloadUntyped) {
        ComponentSource<KT, IT, CT> componentSource = (ComponentSource<KT, IT, CT>) componentSourceUntyped;
        ItemPayload<KT, IT, CT> itemPayload = (ItemPayload<KT, IT, CT>) itemPayloadUntyped;
        final GCInventoryBase<KT, IT, CT> sourceInventory = componentSource.getActor().inventoryBase;
        final KT sourceKey = componentSource.getActor().getKey();
        boolean canDrop = true;
        canDrop &= sourceInventory.canCrossInventoryDropFromHere(sourceKey, itemPayload.getObject(), component.inventoryBase, component.getKey());
        canDrop &= component.inventoryBase.canCrossInventoryDropToHere(sourceInventory, sourceKey, itemPayload.getObject(), component.getKey());
        return canDrop;
    }

    @Override
    public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
        ItemPayload<?, ?, ?> itemPayload = (ItemPayload<?, ?, ?>) payload;
        ComponentSource<?, ?, ?> componentSource = (ComponentSource<?, ?, ?>) source;
        final GCInventoryBase<?, ?, ?> sourceInventory = componentSource.getActor().inventoryBase;
        if (component.inventoryBase == sourceInventory) {
            //noinspection unchecked
            ItemPayload<K, T, C> localPayload = (ItemPayload<K, T, C>) itemPayload;
            final K payloadKey = localPayload.getSourceComponent().getKey();
            if(payloadKey.equals(component.getKey())) {
                return;
            }
            component.inventoryBase.onSameInventoryDrop(payloadKey, localPayload.getObject(), component.getKey());
        } else {
            onCrossInventoryDrop(componentSource, itemPayload);
        }
    }

    @SuppressWarnings({"unchecked", "ConstantConditions"})
    private <KT, IT extends GCInventoryItem, CT extends GCInventorySlotBaseComponent<KT, IT, CT>>
    void onCrossInventoryDrop(ComponentSource<?, ?, ?> componentSourceUntyped, ItemPayload<?, ?, ?> itemPayloadUntyped) {
        ComponentSource<KT, IT, CT> componentSource = (ComponentSource<KT, IT, CT>) componentSourceUntyped;
        ItemPayload<KT, IT, CT> itemPayload = (ItemPayload<KT, IT, CT>) itemPayloadUntyped;
        final GCInventoryBase<KT, IT, CT> sourceInventory = componentSource.getActor().inventoryBase;
        final KT sourceKey = componentSource.getActor().getKey();
        sourceInventory.onCrossInventoryDropFromHere(sourceKey, itemPayload.getObject(), component.inventoryBase, component.getKey());
        component.inventoryBase.onCrossInventoryDropToHere(sourceInventory, sourceKey, itemPayload.getObject(), component.getKey());
    }
}
