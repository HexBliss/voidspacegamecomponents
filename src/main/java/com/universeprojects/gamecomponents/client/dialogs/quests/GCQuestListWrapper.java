package com.universeprojects.gamecomponents.client.dialogs.quests;

import java.util.ArrayList;

/**
 * Used as a container for all the quests. {@link GCQuestListItem}
 */
public class GCQuestListWrapper {
    /**
     * Lists of quests
     */
    private ArrayList<GCQuestListItem> activeQuests;
    private ArrayList<GCQuestListItem> completedQuests;
    private ArrayList<GCQuestListItem> skippedQuests;
    private ArrayList<GCQuestListItem> abandonedQuests;

    /**
     * Constructor for the quest list wrapper
     */
    GCQuestListWrapper () {
        activeQuests    = new ArrayList<>();
        completedQuests = new ArrayList<>();
        skippedQuests   = new ArrayList<>();
        abandonedQuests = new ArrayList<>();
    }

    // Getters begin here ----------------------------------------------------------------------------------------------

    /**
     * @return The list of active quests
     */
    ArrayList<GCQuestListItem> getActiveQuests() {
        return activeQuests;
    }

    /**
     * @return The list of completed quests
     */
    ArrayList<GCQuestListItem> getCompletedQuests() {
        return completedQuests;
    }

    /**
     * @return The list of skipped quests
     */
    ArrayList<GCQuestListItem> getSkippedQuests() {
        return skippedQuests;
    }

    /**
     * @return The list of abandoned quests
     */
    ArrayList<GCQuestListItem> getAbandonedQuests() {
        return abandonedQuests;
    }

    // Getters end here ----------------------------------------------------------------------------------------------

    /**
     * Clear all quests
     */
    public void clear () {
        activeQuests.clear();
        completedQuests.clear();
        skippedQuests.clear();
        abandonedQuests.clear();
    }
}





