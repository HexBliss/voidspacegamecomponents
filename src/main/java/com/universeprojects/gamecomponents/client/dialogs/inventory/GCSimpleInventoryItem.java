package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.common.shared.util.Strings;

import java.util.Collections;
import java.util.Map;

public class GCSimpleInventoryItem implements GCInventoryItem {

    protected String name;
    protected String itemClass;
    protected String description;
    protected int quantity;
    protected boolean stackable;
    protected String iconSpriteKey;
    protected Rarity rarity;
    protected Map<String, String> additionalProperties;

    @Override
    public String getName() {
        return Strings.nullToEmpty(name);
    }

    @Override
    public String getItemClass() {
        return Strings.nullToEmpty(itemClass);
    }

    @Override
    public String getDescription() {
        return Strings.nullToEmpty(description);
    }

    @Override
    public long getQuantity() {
        return quantity;
    }

    @Override
    public boolean isStackable() {
        return stackable;
    }

    @Override
    public String getIconSpriteKey() {
        return iconSpriteKey;
    }

    @Override
    public String getRarityIconStyle() {
        return rarity != null ? rarity.getIconStyle() : Rarity.COMMON.getIconStyle();
    }

    @Override
    public Map<String, String> getAdditionalProperties() {
        return additionalProperties != null ? additionalProperties : Collections.emptyMap();
    }

    @Override
    public void createAdditionalInspectorComponents(Table inspectorTable) {

    }
}
