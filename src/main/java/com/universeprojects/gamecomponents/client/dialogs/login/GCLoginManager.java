package com.universeprojects.gamecomponents.client.dialogs.login;

import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.html5engine.client.framework.H5ELayer;

import java.util.List;

public abstract class GCLoginManager<L extends LoginInfo, S extends ServerInfo, C extends CharacterInfo> {

    public static final String CODE_COMMUNICATION ="communication_error";
    public static final String CODE_LOGIN_FAILED ="bad_login";
    public static final String CODE_JOIN_FAILED ="bad_join";
    public static final String CODE_BAD_GAME = "bad_game";
    private final H5ELayer layer;

    private GCLoginScreen<L> loginScreen;
    private GCServerSelectScreen<L, S> serverSelectScreen;
    private GCCharacterSelectScreen<L, S, C> characterSelectScreen;
    private GCCharacterCreateScreen<L, S> characterCreateScreen;
    private GCTermsOfService termsOfService;
    private GCPrivacyPolicy privacyPolicy;
    private GCRegisterScreen<L> registerScreen;

    protected GCLoginManager(H5ELayer layer) {
        this.layer = layer;
    }

    public void openLoginScreen() {
        if(loginScreen == null) {
            loginScreen = new GCLoginScreen<>(layer, this);
        }
        loginScreen.open();
    }

    public void openServerSelectScreen(L loginInfo) {
        if(serverSelectScreen == null) {
            serverSelectScreen = new GCServerSelectScreen<>(layer, this);
        }
        serverSelectScreen.open(loginInfo);
    }

    public void openCharacterSelectScreen(L loginInfo, S serverInfo) {
        if(characterSelectScreen == null) {
            characterSelectScreen = new GCCharacterSelectScreen<>(layer, this);
        }
        characterSelectScreen.open(loginInfo, serverInfo);
    }

    public void openCharacterCreateScreen(L loginInfo, S serverInfo) {
        if(characterCreateScreen == null) {
            characterCreateScreen = new GCCharacterCreateScreen<>(layer, this);
        }
        characterCreateScreen.open(loginInfo, serverInfo);
    }

    public void openRegisterWindowWithPolicies() {
        openTermsOfServiceWindow();
    }

    public void openTermsOfServiceWindow() {
        if(termsOfService == null) {
            termsOfService = new GCTermsOfService(layer, this);
        }
        termsOfService.open();
    }

    public void openPrivacyPolicyWindow() {
        if(privacyPolicy == null) {
            privacyPolicy = new GCPrivacyPolicy(layer, this);
        }
        privacyPolicy.open();
    }

    public void openRegisterWindow() {
        if(registerScreen == null) {
            registerScreen = new GCRegisterScreen<>(layer, this);
        }
        registerScreen.open();
    }

    public void onSuccessfulLogin(L loginInfo) {
        openServerSelectScreen(loginInfo);
    }

    public void onServerSelected(L loginInfo, S serverInfo) {
        openCharacterSelectScreen(loginInfo, serverInfo);
    }

    protected abstract void login(String email, String password, Callable1Args<L> successCallback, Callable1Args<String> errorCallback);

    protected abstract void register(String email, String password, String userName, Callable1Args<L> successCallback, Callable1Args<String> errorCallback);

    protected abstract void loadPrivacyPolicy(Callable1Args<String> successCallback, Callable1Args<String> errorCallback);

    protected abstract void loadTermsOfService(Callable1Args<String> successCallback, Callable1Args<String> errorCallback);

    protected abstract void loadServers(L loginInfo, Callable1Args<List<S>> successCallback, Callable1Args<String> errorCallback);

    protected abstract void pingServer(S serverInfo, Callable0Args successCallback, Callable1Args<String> errorCallback);

    protected abstract void loadGameCharacters(L loginInfo, S serverInfo, Callable1Args<List<C>> successCallback, Callable1Args<String> errorCallback);

    protected abstract void loadUserCharacters(L loginInfo, Callable1Args<List<String>> successCallback, Callable1Args<String> errorCallback);

    protected abstract void deleteCharacter(L loginInfo, S serverInfo, C characterInfo, Callable0Args successCallback, Callable1Args<String> errorCallback);

    protected abstract void joinWithNewCharacter(L loginInfo, S serverInfo, String characterName, Callable0Args successCallback, Callable1Args<String> errorCallback);

    protected abstract void joinWithExistingCharacter(L loginInfo, S serverInfo, C characterInfo, Callable0Args successCallback, Callable1Args<String> errorCallback);

}
