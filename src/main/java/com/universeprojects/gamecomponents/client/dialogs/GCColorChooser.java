package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.gamecomponents.client.elements.GCSlider;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.drawable.RectShapeDrawable;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;

/**
 * A simple dialog for creating RGB color strings
 *
 * @author J. K. Rowling
 */
public abstract class GCColorChooser {

    private final GCSimpleWindow window;
    private final H5EInputBox fieldRGB;
    private final GCSlider sliderR, sliderG, sliderB;
    private final Color color;

    private final static int FIELD_W = 80;

    private int r, g, b;

    public GCColorChooser(H5ELayer layer) {

        window = new GCSimpleWindow(layer, "color-chooser", "Choose Color");
        window.onClose.registerHandler(this::onCancel);

        color = new Color(Color.WHITE);
        RectShapeDrawable rect = new RectShapeDrawable(color);
        Image colorSample = new Image(rect);
        rect.setMinWidth(100);
        rect.setMinHeight(100);
        window.add(colorSample).left();

        window.defaults().left();

        //////////////////////////////////////
        //   RED
        //

        window.addEmptyLine(15);
        window.addLabel("Red:");

        H5EInputBox fieldR = new H5EInputBox(layer);
        fieldR.setWidth(FIELD_W);
        fieldR.setTypeNumber();
        window.add(fieldR).right();

        sliderR = new GCSlider(layer);
        sliderR.attachInputBox(fieldR);
        sliderR.setRange(0, 255, 1);
        sliderR.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onSliderInput();
                event.handle();
            }
        });
        window.row();
        window.add(sliderR).colspan(2).growX();

        //////////////////////////////////////
        //   GREEN
        //

        window.addEmptyLine(5);
        window.addLabel("Green:");

        H5EInputBox fieldG = new H5EInputBox(layer);
        fieldG.setWidth(FIELD_W);
        fieldG.setTypeNumber();
        window.add(fieldG).right();

        sliderG = new GCSlider(layer);
        sliderG.attachInputBox(fieldG);
        sliderG.setRange(0, 255, 1);
        sliderG.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onSliderInput();
                event.handle();
            }
        });
        window.row();
        window.add(sliderG).colspan(2).growX();


        //////////////////////////////////////
        //   BLUE
        //

        window.addEmptyLine(5);
        window.addLabel("Blue:");

        H5EInputBox fieldB = new H5EInputBox(layer);
        fieldB.setWidth(FIELD_W);
        fieldB.setTypeNumber();
        window.add(fieldB).right();

        sliderB = new GCSlider(layer);
        sliderB.attachInputBox(fieldB);
        sliderB.setRange(0, 255, 1);
        sliderB.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onSliderInput();
                event.handle();
            }
        });
        window.row();
        window.add(sliderB).colspan(2).growX();


        //////////////////////////////////////
        //   R G B
        //

        window.addEmptyLine(5);
        window.addLabel("RGB-Color:");

        fieldRGB = new H5EInputBox(layer);
        fieldRGB.setWidth(FIELD_W);
        fieldRGB.setTypeNumber();
        fieldRGB.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onRgbInput();
                event.handle();
            }
        });
        window.add(fieldRGB).right().padLeft(10);


        //////////////////////////////////////
        //   BUTTONS
        //

        window.addEmptyLine(25);

        H5EButton btnOkay = window.addSmallButton("Okay").left().fillX().uniformX().getActor();
        btnOkay.addButtonListener(() -> {
            window.close();
            onOk(r, g, b);
        });

        H5EButton btnCancel = window.addSmallButton("Cancel").right().fillX().uniformX().getActor();
        btnCancel.addButtonListener(() -> {
            window.close();
            onCancel();
        });
    }

    public void positionProportionally(float propX, float propY) {
        window.positionProportionally(propX, propY);
    }

    public void open() {
        window.open();

        r = getDefaultRed();
        g = getDefaultGreen();
        b = getDefaultBlue();

        sliderR.setValue(r);
        sliderG.setValue(g);
        sliderB.setValue(b);

        applyColor();
    }

    private void applyColor() {
        color.set(r / 255f, g / 255f, b / 255f, 1f);
        fieldRGB.setText(color.toString().substring(0, 6));
    }

    public void close() {
        window.close();
        onCancel();
    }

    public boolean isOpen() {
        return window.isOpen();
    }

    public void onRgbInput() {
        String colorStr = fieldRGB.getText().trim();
        Color fieldColor;
        try {
            fieldColor = Color.valueOf(colorStr);
        } catch (Exception ex) {
            return;
        }
        r = (int) (fieldColor.r * 255);
        g = (int) (fieldColor.g * 255);
        b = (int) (fieldColor.b * 255);

        sliderR.setValue(r);
        sliderG.setValue(g);
        sliderB.setValue(b);

        this.color.set(fieldColor);
    }

    public void onSliderInput() {
        r = (int) sliderR.getValue();
        g = (int) sliderG.getValue();
        b = (int) sliderB.getValue();

        applyColor();
    }

    protected abstract int getDefaultRed();

    protected abstract int getDefaultGreen();

    protected abstract int getDefaultBlue();

    protected abstract void onCancel();

    protected abstract void onOk(int r, int g, int b);
}
