package com.universeprojects.gamecomponents.client.dialogs;

import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

/**
 * Creates a generic UI for players to confirm a decision. UI components that need a confirmation should implement
 * {@link Confirmable} as this screen will call the implemented method after the user confirms/denies their decision.
 */
public class GCConfirmationScreen {

    private GCWindow confirmationWindow;
    private boolean confirmed;
    private Confirmable thingToConfirm;
    private H5ELabel deleteWarningText;

    public GCConfirmationScreen(H5ELayer layer, Confirmable thingToConfirm,String confirmationText ) {
        this.thingToConfirm=thingToConfirm;
        confirmationWindow = new GCWindow(layer, 444, 300);
        confirmationWindow.positionProportionally(0.5f,0.5f);
        confirmationWindow.setTitle("Are You Sure?");
        confirmationWindow.open();
        confirmationWindow.close();
        H5EButton btnConfirm = new H5EButton("Yes",layer);
        btnConfirm.addButtonListener(this::confirmed);
        H5EButton btnBack=new H5EButton("No",layer);
        btnBack.addButtonListener(this::openConfirmedWindow);
        deleteWarningText = new H5ELabel(confirmationText,layer);
        confirmationWindow.add(deleteWarningText);
        confirmationWindow.row();
        confirmationWindow.add(btnConfirm);
        confirmationWindow.row().padTop(10);
        confirmationWindow.add(btnBack);
    }

    /**
     * Called when the user clicks "yes" to confirm their decision.
     */
    public void  confirmed(){
        confirmed=true;
        openConfirmedWindow();
    }
    public void openConfirmedWindow (){
        thingToConfirm.confirmed();
        this.close();
    }

    /**
     * This method should be called after the user has selected an option.
     * @return true if the user selected yes, otherwise returns false.
     */
    public boolean isConfirmed(){
        return confirmed;
    }

    /**
     * Change the message of the confirmation screen.
     * @param newText
     */
    public void setText(String newText){
        deleteWarningText.setText(newText);
    }
    public void setTitle(String newText){
        confirmationWindow.setTitle(newText);
    }

    public void open() {
        confirmed =false;
        confirmationWindow.open();
    }

    public void close() {
        confirmationWindow.close();
    }

    public boolean isOpen() {
        return confirmationWindow.isOpen();
    }

    public void positionProportionally(float propX, float propY) {
        confirmationWindow.positionProportionally(propX, propY);
    }

}
