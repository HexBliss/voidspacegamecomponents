package com.universeprojects.gamecomponents.client.dialogs.invention;

import java.util.ArrayList;
import java.util.List;

public class GCIdeasData {

    private final List<GCIdeaDataItem> items = new ArrayList<>();

    private GCIdeasData() {
    }

    public static GCIdeasData createNew() {
        return new GCIdeasData();
    }

    public GCIdeasData add(GCIdeasData other) {
        for (GCIdeaDataItem item : other.items) {
            // we call the add() method sequentially in order for new indices to be assigned
            add(item.id, item.name, item.description, item.iconKey);
        }
        return this;
    }

    public GCIdeasData add(Long id, String name, String description, String iconKey) {
        int index = items.size();
        items.add(new GCIdeaDataItem(index, id, name, description, iconKey));
        return this;
    }

    public GCIdeaDataItem get(int index) {
        return items.get(index);
    }

    public int size() {
        return items.size();
    }

    public static class GCIdeaDataItem {
        final int index;

        public Long id;
        public String name;
        final String description;
        public String iconKey;

        GCIdeaDataItem(int index, Long id, String name, String description, String iconKey) {
            this.index = index;

            this.id = id;
            this.name = name;
            this.description = description;
            this.iconKey = iconKey;
        }

        @Override
        public String toString() {
            return new StringBuilder()
                .append("index: ").append(index).append(", ")
                .append("id: ").append(id).append(", ")
                .append("name: ").append(name).append(", ")
                .append("description: ").append(description).append(", ")
                .append("iconKey: ").append(iconKey).append(", ")
                .toString();

        }
    }

}


