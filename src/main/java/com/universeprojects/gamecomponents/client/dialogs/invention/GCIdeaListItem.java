package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCIdeasData.GCIdeaDataItem;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

@SuppressWarnings("FieldCanBeLocal")
class GCIdeaListItem extends GCListItem {

    public static final int ICON_SIZE = 64;
    final GCIdeaDataItem data;

    private final H5ELabel nameLabel;
    private final H5ELabel descTextArea;

    public GCIdeaListItem(H5ELayer layer, GCListItemActionHandler<GCIdeaListItem> actionHandler, GCIdeaDataItem data) {
        super(layer, actionHandler);

        left().top();
        defaults().left().top();

        if (data.index < 0) {
            throw new IllegalArgumentException("Index can't be negative");
        }
        if (Strings.isEmpty(data.name)) {
            throw new IllegalArgumentException("Idea name can't be blank");
        }
        if (Strings.isEmpty(data.description)) {
            throw new IllegalArgumentException("Idea description can't be blank");
        }
        if (Strings.isEmpty(data.iconKey)) {
            throw new IllegalArgumentException("Idea icon key can't be blank");
        }

        this.data = Dev.checkNotNull(data);

        add(H5EIcon.fromSpriteType(layer, data.iconKey)).size(ICON_SIZE).padRight(5).getActor();

        Table subTable = new Table();
        add(subTable).growX().fillY().padBottom(5);
        subTable.left().top();
        subTable.defaults().left().top();

        nameLabel = subTable.add(new H5ELabel(layer)).getActor();
        nameLabel.setFontScale(1f);
        nameLabel.setColor(Color.valueOf("#FAFAFA"));
        nameLabel.setText(data.name);

        subTable.row();

        descTextArea = subTable.add(new H5ELabel(layer)).growX().fillY().getActor();
        descTextArea.setFontScale(0.8f);
        descTextArea.setColor(Color.valueOf("#CCCCCC"));
        descTextArea.setWrap(true);
        descTextArea.setText(data.description);
        descTextArea.setTouchable(Touchable.disabled);
    }

}
