package com.universeprojects.gamecomponents.client.dialogs.inventory;

public interface SlotConfig {
    String getName();
    String getEmptySlotIcon();
    String getDescription();
}
