package com.universeprojects.gamecomponents.client.dialogs;

import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;

public abstract class GCInventoryItemActionHandler extends GCListItemActionHandler<GCInventoryListItem> {

    abstract protected void handleActionBtn(GCInventoryListItem item, String actionButtonText);

}
