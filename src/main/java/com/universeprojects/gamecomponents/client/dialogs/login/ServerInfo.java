package com.universeprojects.gamecomponents.client.dialogs.login;

public interface ServerInfo {
    String getName();
    int getPopulation();
    Integer getMaxPopulation();
}
