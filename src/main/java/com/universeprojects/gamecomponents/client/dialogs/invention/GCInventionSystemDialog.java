package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EProgressBar;
import com.universeprojects.html5engine.shared.GenericEvent;

import java.util.LinkedHashMap;
import java.util.Map;

@SuppressWarnings("FieldCanBeLocal")
public abstract class GCInventionSystemDialog {

    final H5ELayer layer;
    final GCWindow window;

    private final H5EButton btnKnowledge;
    private final H5EButton btnExperiments;
    private final H5EButton btnIdeas;
    private final H5EButton btnBuildItem;
    private final H5EButton btnBuildBuilding;
    private final ButtonGroup<H5EButton> buttons;

    protected final H5EButton btnCancel;
    protected final H5EProgressBar embeddedProgressBar;
    protected boolean maximumReached;

    public final H5EStack lowerButtonRow;
    public final H5EStack leftContent;
    public final H5EStack rightContent;
    public final H5EStack rightTitleArea;
    public final H5EStack progressBarArea;

    protected final GCInventionKnowledgeTab knowledgeTab;
    protected final GCInventionExperimentsTab experimentsTab;
    protected final GCInventionIdeasTab ideasTab;
    protected final GCInventionItemsTab itemsTab;
    protected final GCInventionBuildingsTab buildingsTab;
    protected final GCInventionRecipeTab recipeTab;
    private final Map<GCInventionTab, H5EButton> tabsAndButtons = new LinkedHashMap<>();
    private GCInventionTab currentTab;

    private GCLoadingIndicator loadingIndicator;

    private final Logger log = Logger.getLogger(GCInventionSystemDialog.class);

    public GCInventionSystemDialog(H5ELayer layer) {
        this.layer = Dev.checkNotNull(layer);

        window = new GCWindow(layer, 800, 600, "ui2-invention-window");
        window.setId("invention-system-control");
        window.defaults().left().top();
        loadingIndicator = new GCLoadingIndicator(layer);

        final Table leftArea = window.add(new Table()).left().top().growY().width(320).getActor();
        final Table rightArea = window.add(new Table()).left().top().grow().getActor();
        rightArea.top().left();
        rightTitleArea = rightArea.add(new H5EStack(layer)).left().top().growX().getActor();
        rightArea.row();
        rightContent = rightArea.add(new H5EStack(layer)).left().top().grow().getActor();

        final HorizontalGroup horizontalGroup = leftArea.add(new HorizontalGroup()).
            padTop(24).padBottom(36).left().getActor();
        horizontalGroup.space(0).padLeft(23);

        btnKnowledge = ButtonBuilder.inLayer(layer)
            .withStyle("button2-tab")
            .withForegroundSpriteType("tab-knowledge")
            .withName("tab-btn-knowledge")
            .build();
        btnExperiments = ButtonBuilder.inLayer(layer)
            .withStyle("button2-tab")
            .withForegroundSpriteType("tab-experiments")
            .withName("tab-btn-experiments")
            .build();
        btnIdeas = ButtonBuilder.inLayer(layer)
            .withStyle("button2-tab")
            .withForegroundSpriteType("tab-ideas")
            .withName("tab-btn-ideas")
            .build();
        btnBuildItem = ButtonBuilder.inLayer(layer)
            .withStyle("button2-tab")
            .withForegroundSpriteType("tab-build-item")
            .withName("tab-btn-buildItem")
            .build();
        btnBuildBuilding = ButtonBuilder.inLayer(layer)
            .withStyle("button2-tab")
            .withForegroundSpriteType("tab-build-building")
            .withName("tab-btn-buildBuilding")
            .build();
        final H5EButton[] buttonArray = {btnKnowledge, btnExperiments, btnIdeas, btnBuildItem, btnBuildBuilding};

        final int TAB_BUTTON_WIDTH  = 30;
        final int TAB_BUTTON_HEIGHT = 25;

        for (H5EButton btn : buttonArray) {
            horizontalGroup.addActor(btn);
            btn.getImageCell()
                    .width(TAB_BUTTON_WIDTH)
                    .height(TAB_BUTTON_HEIGHT)
                    .align(Align.center);
        }
        buttons = new ButtonGroup<>(buttonArray);
        buttons.setMaxCheckCount(1);
        buttons.setMinCheckCount(1);

        leftArea.row();

        leftContent = leftArea.add(new H5EStack(layer)).grow().getActor();

        leftArea.row();

        lowerButtonRow = leftArea.add(new H5EStack(layer)).fillX().left().padLeft(43).padBottom(4).width(232).height(30).getActor();

        knowledgeTab = addTab(new GCInventionKnowledgeTab(this), btnKnowledge);
        experimentsTab = addTab(new GCInventionExperimentsTab(this), btnExperiments);
        ideasTab = addTab(new GCInventionIdeasTab(this), btnIdeas);
        itemsTab = addTab(new GCInventionItemsTab(this), btnBuildItem);
        buildingsTab = addTab(new GCInventionBuildingsTab(this), btnBuildBuilding);
        recipeTab = addTab(new GCInventionRecipeTab(this), null); // no button associated

        btnKnowledge.addCheckedButtonListener(this::loadDataForKnowledgeTab);
        btnExperiments.addCheckedButtonListener(this::loadDataForExperimentsTab);
        btnIdeas.addCheckedButtonListener(this::loadDataForIdeasTab);
        btnBuildItem.addCheckedButtonListener(this::loadDataForItemsTab);
        btnBuildBuilding.addCheckedButtonListener(this::loadDataForBuildingsTab);

        btnCancel = ButtonBuilder.inLayer(layer).withStyle("button-red").withText("Cancel").build();
        btnCancel.addButtonListener(() -> onCancelBtnPressed());
        btnCancel.setVisible(false);
        lowerButtonRow.add(btnCancel);

        rightArea.row();
        progressBarArea = rightArea.add(new H5EStack(layer)).fillX().right().padTop(10).align(Align.center).getActor();
        embeddedProgressBar = new H5EProgressBar(progressBarArea.getLayer());
        progressBarArea.add(embeddedProgressBar);
        embeddedProgressBar.setRange(0, 1000);
        embeddedProgressBar.setVisible(false);
        maximumReached = false;

        clear();
    }

    private <T extends GCInventionTab> T addTab(T tab, H5EButton btn) {
        tabsAndButtons.put(tab, btn);
        return tab;
    }

    public void clear() {
        for (GCInventionTab tab : tabsAndButtons.keySet()) {
            tab.deactivate();
        }
        currentTab = knowledgeTab;
        btnKnowledge.setProgrammaticChangeEvents(false);
        btnKnowledge.setChecked(true);
        btnKnowledge.setProgrammaticChangeEvents(true);
    }

    protected void openTab(GCInventionTab tab) {
        Dev.checkNotNull(tab);

        H5EButton btn = tabsAndButtons.get(tab);
        if (btn != null) {
            btn.setProgrammaticChangeEvents(false);
            btn.setChecked(true);
            btn.setProgrammaticChangeEvents(true);
        }

        if (currentTab != tab) {
            currentTab.deactivate();
        }
        currentTab = tab;

        tab.show();
    }

    protected boolean isCurrentTab(GCInventionTab tab) {
        return currentTab == tab;
    }

    public void open() {
        window.open();
        reloadCurrentTab();
    }

    public void close() {
        window.close();
    }

    public void onCloseRegisterHandler (GenericEvent.GenericEventHandler0Args handler) {
        window.onClose.registerHandler(handler);
    }

    public boolean isOpen() {
        return window.isOpen();
    }

    public void positionProportionally(float propX, float propY) {
        window.positionProportionally(propX, propY);
    }

    public void openKnowledgeTab(GCKnowledgeData knowledgeData) {
        clear();
        openTab(knowledgeTab);
        knowledgeTab.populateData(knowledgeData);
        finishLoading();
    }

    public void openExperimentsTab(GCInventoryData experimentsData) {
        openTab(experimentsTab);
        experimentsTab.populateData(experimentsData);
        finishLoading();
    }

    public void openIdeasTab(GCIdeasData ideasData) {
        openTab(ideasTab);
        ideasTab.populateData(ideasData);
        finishLoading();
    }

    public void openItemsTab(GCSkillsData itemsData) {
        openTab(itemsTab);
        itemsTab.populateData(itemsData);
        finishLoading();
    }

    public void openBuildingsTab(GCSkillsData buildingsData) {
        openTab(buildingsTab);
        buildingsTab.populateData(buildingsData);
        finishLoading();
    }

    public void openRecipeTab(GCRecipeData recipeData) {
        openTab(recipeTab);
        recipeTab.initRecipeData(recipeData);
        recipeTab.resetRepeats();
        finishLoading();
    }

    public void updateRecipeTab(GCRecipeData recipeData) {
        if (currentTab == recipeTab) {
            recipeTab.updateRecipeData(recipeData);
        }
    }

    public void updateExperimentsTab(GCInventoryData inventoryData) {
        if (currentTab == experimentsTab) {
            experimentsTab.populateData(inventoryData);
        }
    }

    private void reloadCurrentTab() {
        if (currentTab == knowledgeTab) {
            loadDataForKnowledgeTab();
        } else if (currentTab == experimentsTab) {
            loadDataForExperimentsTab();
        } else if (currentTab == ideasTab) {
            loadDataForIdeasTab();
        } else if (currentTab == itemsTab) {
            loadDataForItemsTab();
        } else if (currentTab == buildingsTab) {
            loadDataForBuildingsTab();
        }
    }

    protected void flagLoading() {
        loadingIndicator.activate(window);
    }

    protected void finishLoading () {
        loadingIndicator.deactivate();
    }

    //////////////////////////////////////////////////////////////
    //     CLIENT INTEGRATION INTERFACE BELOW
    //

    /**
     * This hook signals that the user requested to open the "knowledge" tab.
     * The implementer's responsibility is to load the relevant data and then call {@link #openKnowledgeTab}.
     */
    public abstract void loadDataForKnowledgeTab();

    /**
     * This hook signals that the user requested to open the "experiments" tab.
     * The implementer's responsibility is to load the relevant data and then call {@link #openExperimentsTab}.
     */
    public abstract void loadDataForExperimentsTab();

    /**
     * This hook signals that the user requested to open the "ideas" tab.
     * The implementer's responsibility is to load the relevant data and then call {@link #openIdeasTab}.
     */
    public abstract void loadDataForIdeasTab();

    /**
     * This hook signals that the user requested to open the "build-item" tab.
     * The implementer's responsibility is to load the relevant data and then call {@link #openItemsTab}.
     */
    public abstract void loadDataForItemsTab();

    /**
     * This hook signals that the user requested to open the "build-building" tab.
     * The implementer's responsibility is to load the relevant data and then call {@link #openBuildingsTab}.
     */
    public abstract void loadDataForBuildingsTab();

    public abstract void onBeginExperimentsBtnPressed(Integer selectionIdx, int repeats);

    public abstract void onBeginPrototypingBtnPressed(Long ideaId);

    public abstract void onBuildItemBtnPressed(Long skillId);

    public abstract void onDeleteSkillBtnPressed(Long skillId);

    public abstract void onBuildBuildingBtnPressed(Long skillId);

    public abstract void onRecipeOkayBtnPressed(GCRecipeData recipe, int repeats);

    public abstract void onIconInfoClicked(String uid);

    public abstract void showCancelBtn();

    public abstract void hideCancelBtn();

    public abstract void onCancelBtnPressed();

    public abstract void showEmbeddedProgressBar();

    public abstract void hideEmbeddedProgressBar();

    public abstract void setEmbeddedProgressBarMaximum(int maximum);

    public abstract void setEmbeddedProgressBarProgress(int progress);

    public abstract void clearEmbeddedProgressBar();
}
