package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.universeprojects.html5engine.shared.abstractFramework.MarkerRectangle;

public class GCEquipmentSlotComponent<S extends Comparable<S>, T extends GCInventoryItem> extends GCInventorySlotComponent<EquipmentSlotKey<S>, T, GCEquipmentSlotComponent<S, T>> {

    private final MarkerRectangle marker;

    public GCEquipmentSlotComponent(GCInventoryBase<EquipmentSlotKey<S>, T, GCEquipmentSlotComponent<S, T>> inventoryBase, EquipmentSlotKey<S> key, MarkerRectangle marker, SlotConfig slotConfig) {
        super(inventoryBase, key, slotConfig);
        this.marker = marker;
        updateSize();
    }

    protected void updateSize() {
        setWidth(getPrefWidth());
        setHeight(getPrefHeight());
    }

    public MarkerRectangle getMarker() {
        return marker;
    }

    @Override
    public void setIconSize(float iconSize) {
        super.setIconSize(iconSize);
        updateSize();
    }

    @Override
    public String getEmptySpriteKey() {
        return "images/icons/base/junk_grade_2.png";
    }

    @Override
    public String getEmptyFrameIconStyle() {
        return "icon-equipment_item_frame";
    }

    @Override
    public String getFrameStyle() {
        return "icon-equipment_item_frame";
    }

    @Override
    public int getFrameBaseIconSize() {
        return GCItemIcon.BASE_EMPTY_FRAME_ICON_SIZE;
    }
}