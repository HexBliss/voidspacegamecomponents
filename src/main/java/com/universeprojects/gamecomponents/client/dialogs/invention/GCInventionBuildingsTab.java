package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

class GCInventionBuildingsTab extends GCInventionTab {

    private final Logger log = Logger.getLogger(GCInventionBuildingsTab.class);

    private final H5EButton btnBuildBuilding;

    protected GCInventionBuildingsTab(GCInventionSystemDialog dialog) {
        super(dialog, "Building construction");

        btnBuildBuilding = ButtonBuilder.inLayer(layer).withStyle("button2").withText("Build Building").build();
        btnBuildBuilding.addButtonListener(() -> {
            // TODO: this needs to pass the ID of the selected skill
            dialog.onBuildBuildingBtnPressed(null);
        });
        lowerButtonRow.add(btnBuildBuilding).grow();
    }

    @Override
    protected void clearTab() {

    }

    void populateData(GCSkillsData buildingsData) {

    }

}
