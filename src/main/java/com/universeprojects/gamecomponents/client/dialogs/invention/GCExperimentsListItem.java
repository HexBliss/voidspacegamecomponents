package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData.GCInventoryDataItem;
import com.universeprojects.gamecomponents.client.elements.GCLabel;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;


@SuppressWarnings("FieldCanBeLocal")
public class GCExperimentsListItem extends GCListItem {

    public final GCInventoryDataItem data;

    private final H5EIcon icon;
    private final H5EIcon infoIcon;
    private final H5ELabel quantityLabel;
    private final GCLabel nameLabel;
    public static final int ICON_SIZE = 64;

    public GCExperimentsListItem(H5ELayer layer, GCListItemActionHandler<GCExperimentsListItem> actionHandler, GCInventoryDataItem data) {
        super(layer, actionHandler);

        if (data.index < 0) {
            throw new IllegalArgumentException("Index can't be negative");
        }
        if (Strings.isEmpty(data.name)) {
            throw new IllegalArgumentException("Item name can't be blank");
        }
        if (data.quantity <= 0) {
            throw new IllegalArgumentException("Quantity must be greater than 0");
        }

        this.data = data;

        if (Strings.isEmpty(data.iconSpriteKey)) {
            icon = null;
            infoIcon = null;
        } else {
            H5EStack stack = new H5EStack(layer);
            add(stack).left().top().padRight(5).size(ICON_SIZE);
            icon = H5EIcon.fromSpriteType(layer, data.iconSpriteKey);
            stack.add(icon);
            icon.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    actionHandler.onInfoButtonClicked(data);
                    event.handle();
                }
            });

            infoIcon = H5EIcon.fromSpriteType(layer, "inspect-indicator", Scaling.none, Align.topRight);
            infoIcon.setTouchable(Touchable.disabled);

            stack.add(infoIcon);
        }

        if (data.quantity == 1) {
            quantityLabel = null;
        } else {
            quantityLabel = add(new H5ELabel(layer)).left().padRight(10).getActor();
            quantityLabel.setText(Integer.toString(data.quantity));
            quantityLabel.setColor(Color.valueOf("#FFFFFF"));
        }

        nameLabel = add(new GCLabel(layer)).left().getActor();
        nameLabel.setText(data.name);
        nameLabel.setColor(Color.valueOf("#FFFFFF"));
    }

}
