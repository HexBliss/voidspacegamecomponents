package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.common.shared.util.RandomUtils;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData.GCInventoryDataItem;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;


@SuppressWarnings("FieldCanBeLocal")
public class GCInventoryListItem extends GCListItem {

    public static final int ICON_SIZE = 50;
    public static final int EMPTY_ICON_SIZE = 44;
    public static final int FRAME_ICON_SIZE = 46;
    public static final int ITEM_ICON_SIZE = 38;
    final GCInventoryDataItem data;
    private H5ELabel quantityLabel;
    private H5EIcon rarityIcon;
    private H5EIcon emptyIcon;
    private H5EIcon itemIcon;
    private H5EIcon frameIcon;
    public static final int LABEL_OFFSET_X = 8;
    public static final int LABEL_OFFSET_Y = 4;

    public GCInventoryListItem(H5ELayer layer, GCListItemActionHandler<GCInventoryListItem> actionHandler, GCInventoryDataItem data) {
        super(layer, actionHandler, "gc-list-blank");

        center();
        defaults().center();

        if(data != null) {
            if (data.index < 0) {
                throw new IllegalArgumentException("Index can't be negative");
            }
            if (Strings.isEmpty(data.name)) {
                throw new IllegalArgumentException("Idea name can't be blank");
            }
            if (Strings.isEmpty(data.iconSpriteKey)) {
                throw new IllegalArgumentException("Idea icon key can't be blank");
            }
        }

        this.data = data;

        final WidgetGroup stack = new WidgetGroup() {
            @Override
            public void layout() {
                handleLayout();
            }
        };

        add(stack).size(ICON_SIZE);

        if(data != null) {
            rarityIcon = H5EIcon.fromStyle(layer, getRarityStyle(data));
            stack.addActor(rarityIcon);
            frameIcon = H5EIcon.fromStyle(layer, "icon-item_frame");
            stack.addActor(frameIcon);
            itemIcon = H5EIcon.fromSpriteType(layer, data.iconSpriteKey, Scaling.stretch, Align.center);
            stack.addActor(itemIcon);
            quantityLabel = new H5ELabel(layer);
            quantityLabel.setStyle(layer.getEngine().getSkin().get("label-small", Label.LabelStyle.class));
            quantityLabel.setText(getQuantity(data));
            stack.addActor(quantityLabel);
        } else {
            emptyIcon = H5EIcon.fromStyle(layer, "icon-bg_empty");
            stack.addActor(emptyIcon);
        }
    }

    private String getQuantity(GCInventoryDataItem data) {
        return String.valueOf(data.quantity);
    }

    private void handleLayout() {
        if(rarityIcon != null) {
            rarityIcon.setSize(ICON_SIZE, ICON_SIZE);
            rarityIcon.validate();
        }
        if(emptyIcon != null) {
            emptyIcon.setSize(EMPTY_ICON_SIZE, EMPTY_ICON_SIZE);
            final int emptyOffset = (ICON_SIZE - EMPTY_ICON_SIZE) / 2;
            emptyIcon.setPosition(emptyOffset, emptyOffset);
            emptyIcon.validate();
        }
        if(frameIcon != null) {
            frameIcon.setSize(FRAME_ICON_SIZE, FRAME_ICON_SIZE);
            final int frameOffset = (ICON_SIZE - FRAME_ICON_SIZE) / 2;
            frameIcon.setPosition(frameOffset, frameOffset);
            frameIcon.validate();
        }
        if(itemIcon != null) {
            itemIcon.setSize(ITEM_ICON_SIZE, ITEM_ICON_SIZE);
            final int itemOffset = (ICON_SIZE - ITEM_ICON_SIZE) / 2;
            itemIcon.setPosition(itemOffset, itemOffset);
            itemIcon.validate();
        }
        if(quantityLabel != null) {
            final int offset = ICON_SIZE - LABEL_OFFSET_X;
            quantityLabel.setSize(quantityLabel.getPrefWidth(), quantityLabel.getPrefHeight());
            quantityLabel.setPosition(offset, LABEL_OFFSET_Y, Align.bottomRight);
            quantityLabel.validate();
        }
    }

    @Override
    public float getPrefWidth() {
        return ICON_SIZE;
    }

    @Override
    public float getPrefHeight() {
        return ICON_SIZE;
    }

    private String getRarityStyle(GCInventoryDataItem data) {
        String[] styles = new String[]{"junk_grade", "common_grade",
            "uncommon_grade", "rare_grade", "epic_grade", "legendary_grade"};
        final int index = RandomUtils.random(styles.length - 1);
        return "icon-"+ styles[index];
    }

    @Deprecated
    public int getIndex() {
        return data.index;
    }

    @Deprecated
    public String getIconSpriteKey() {
        return data.iconSpriteKey;
    }

    @Deprecated
    public String getName() {
        return data.name;
    }

    @Deprecated
    public int getQuantity() {
        return data.quantity;
    }


}
