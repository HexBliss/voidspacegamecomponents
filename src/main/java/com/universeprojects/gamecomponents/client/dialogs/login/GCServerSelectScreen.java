package com.universeprojects.gamecomponents.client.dialogs.login;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

/**
 * Creates the UI for players to login.
 */
public class GCServerSelectScreen<L extends LoginInfo, S extends ServerInfo> {
    final H5ELayer layer;
    final GCWindow window;
    private final GCList<GCServer<S>> serverListing;
    private final GCListItemActionHandler<GCServer<S>> actionHandler;
    private final GCLoadingIndicator loadingIndicator;

    private L loginInfo;

    private final GCLoginManager<L, S, ?> loginManager;
    private final H5EButton btnConnect;

    public GCServerSelectScreen(H5ELayer layer, GCLoginManager<L, S, ?> loginManager) {
        this.layer = Dev.checkNotNull(layer);
        window = new GCWindow(layer, 500, 600, "login-window");
        this.loginManager = loginManager;
        loadingIndicator = new GCLoadingIndicator(layer);
        //window.debug();
        window.setId("server-select");
        window.setTitle("Server Select");
        window.defaults().left().top();
        window.setCloseButtonEnabled(false);
        window.padTop(40);
        window.getTitleTable().padRight(20);

        final Table labelTable = window.add(new Table()).left().top().height(75).fillX().getActor();
        H5ELabel serverName = new H5ELabel("Server Name", layer);

        H5ELabel population = new H5ELabel("Population", layer);

        labelTable.add(serverName).left().expand().padLeft(20);
        labelTable.add(population).right().expandX().padRight(20);
        window.row();

        final H5EScrollablePane serverScrollPane = new H5EScrollablePane(window.getLayer(), "scrollpane-backing-1");

        serverScrollPane.setScrollingDisabled(true, false);
        window.add(serverScrollPane).width(450).expandY().fillY();
        serverListing = new GCList<>(layer, 1, 5, 5, true);
        serverScrollPane.add(serverListing).width(425).expandY().fillY();

        final H5EButton btnBack = new H5EButton("Back", layer);
        btnConnect = new H5EButton("Connect", layer);
        btnConnect.addButtonListener(this::connect);
        btnConnect.setDisabled(true);
        final H5EButton btnRefresh = new H5EButton("Refresh", layer);
        btnRefresh.addButtonListener(this::refreshServerList);
        btnBack.addButtonListener(this::handleBack);
        Table buttonTable = new Table();
        window.row();
        window.add(buttonTable).colspan(2).center();
        buttonTable.add(btnBack).width(125);
        buttonTable.add(btnRefresh).width(125);
        buttonTable.add(btnConnect).width(125);

        this.actionHandler = new GCListItemActionHandler.SimpleHandler<>(this::handleSelection, this::onDoubleClick);
//        window.pack();
        window.positionProportionally(0.5f, 0.5f);
    }

    public void refreshServerList() {
        serverListing.clear();
        loadingIndicator.activate(window);
        loginManager.loadServers(loginInfo, (servers) -> {
            loadingIndicator.deactivate();
            serverListing.clear();
            for(S server : servers) {
                addServer(server);
            }
        }, (error) -> {
            loadingIndicator.deactivate();
            //TODO
        });
    }

    private void onDoubleClick(GCServer<S> item) {
        if(item != null) {
            handleConnect(item.getServerInfo());
        }
    }

    public void addServer(S serverInfo) {
        GCServer<S> server = new GCServer<>(layer, actionHandler, serverInfo);
        serverListing.addItem(server);
    }

    private void connect() {
        final GCServer<S> server = serverListing.getButtonGroup().getChecked();
        if(server != null) {
            handleConnect(server.getServerInfo());
        }
    }

    @SuppressWarnings("unused")
    private void handleSelection(GCServer<S> lastSelectedItem) {
        boolean anySelected = serverListing.getButtonGroup().getChecked() != null;
        btnConnect.setDisabled(!anySelected);
    }

    protected void handleConnect(S serverInfo) {
        loadingIndicator.activate(window);
        loginManager.pingServer(serverInfo, () -> {
            loadingIndicator.deactivate();
            close();
            loginManager.onServerSelected(loginInfo, serverInfo);
        }, (error) -> {
            loadingIndicator.deactivate();
            //TODO
        });

    }

    protected void handleBack() {
        close();
        loginManager.openLoginScreen();
    }

    public void open(L loginInfo) {
        this.loginInfo = loginInfo;
        refreshServerList();
        window.open();
    }

    public void close() {
        window.close();
    }

    public void destroy() {
        window.destroy();
    }

    public boolean isOpen() {
        return window.isOpen();
    }
}





