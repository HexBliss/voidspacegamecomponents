package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.Map;

public interface GCInventoryItem {
    String getName();

    String getItemClass();

    String getDescription();

    long getQuantity();

    boolean isStackable();

    String getIconSpriteKey();

    String getRarityIconStyle();

    Map<String, String> getAdditionalProperties();

    void createAdditionalInspectorComponents(Table inspectorTable);
}
