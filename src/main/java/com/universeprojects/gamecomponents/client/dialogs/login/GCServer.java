package com.universeprojects.gamecomponents.client.dialogs.login;


import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.elements.GCLabel;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

/**
 * Creates the UI for players to login.
 */
public class GCServer<T extends ServerInfo> extends GCListItem {

    private final String serverName;
    private final int population;
    private final T serverInfo;

    public GCServer(H5ELayer layer, GCListItemActionHandler<GCServer<T>> actionHandler, T serverInfo){
        super(layer,actionHandler);
        this.serverName=serverInfo.getName();
        this.population =serverInfo.getPopulation();
        this.serverInfo = serverInfo;
        final H5ELabel nameLabel = add(new GCLabel(layer)).left().growX().padLeft(5).getActor();
        nameLabel.setText(serverName);
        nameLabel.setAlignment(Align.left);


        final H5ELabel populationLabel = add(new GCLabel(layer)).left().growX().padLeft(5).getActor();
        if(serverInfo.getMaxPopulation() != null) {
            populationLabel.setText(this.population +"/"+serverInfo.getMaxPopulation());
        } else {
            populationLabel.setText(String.valueOf(this.population));
        }
        populationLabel.setAlignment(Align.right);
    }

    public T getServerInfo() {
        return serverInfo;
    }

    @Override
    public String toString(){
        return (serverName+"            "+ population);
    }
}





