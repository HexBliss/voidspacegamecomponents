package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.math.UPVector;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.shared.abstractFramework.Marker;
import com.universeprojects.html5engine.shared.abstractFramework.MarkerRectangle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public abstract class GCEquipmentDialog<S extends Comparable<S>, T extends GCInventoryItem> extends GCInventoryBase<EquipmentSlotKey<S>, T, GCEquipmentSlotComponent<S, T>> {

    private final Map<EquipmentSlotKey<S>, T> slotMap = new LinkedHashMap<>();
    private final Map<EquipmentSlotKey<S>, SlotConfig> slotConfigMap = new LinkedHashMap<>();
    private final Map<EquipmentSlotKey<S>, MarkerRectangle> markerMap = new LinkedHashMap<>();
    private GCEquipmentInspector<S, T> inspector;
    private H5ESpriteType spriteType;
    private final Image image;

    public GCEquipmentDialog(H5ELayer layer) {
        super(layer);
        this.image = new Image();
        image.setScaling(Scaling.fit);
    }

    public void updateItems(Map<EquipmentSlotKey<S>, T> slotData) {
        for(Map.Entry<EquipmentSlotKey<S>, T> entry : slotMap.entrySet()) {
            entry.setValue(null);
        }
        for (Map.Entry<EquipmentSlotKey<S>, T> entry : slotData.entrySet()) {
            if (slotMap.containsKey(entry.getKey())) {
                slotMap.put(entry.getKey(), entry.getValue());
            }
        }
        invalidateItems();
    }

    /**
     * Updates the sprite of the equipment dialog and sets up the slotMap keys to be used when populating the
     * equipment dialog slots with items
     * @param spriteType The type of sprite to be used in the equipment dialog
     */
    protected void updateSpriteType(H5ESpriteType spriteType) {
        if (spriteType != this.spriteType) {
            this.spriteType = spriteType;
            image.setDrawable(new TextureRegionDrawable(spriteType.getTextureRegion()));
        }
        resetGraphics();

        // Setting the keys for equipment
        Map<String, List<MarkerRectangle>> typeToMarkerMap = new TreeMap<>();
        for (Marker marker : spriteType.getMarkers()) {
            if (!(marker instanceof MarkerRectangle)) continue;
            List<MarkerRectangle> markers = typeToMarkerMap.get(marker.getTypeLabel());
            if(markers == null) {
                markers = new ArrayList<>();
                typeToMarkerMap.put(marker.getTypeLabel(), markers);
            }
            markers.add((MarkerRectangle) marker);
        }
        slotMap.clear();
        markerMap.clear();
        this.slotConfigMap.clear();
        Map<EquipmentSlotKey<S>, MarkerRectangle> slotKeyMap = new LinkedHashMap<>();
        Map<EquipmentSlotKey<S>, SlotConfig> slotConfigMap = new LinkedHashMap<>();
        generateSlotKeyMap(typeToMarkerMap, slotKeyMap, slotConfigMap);
        for (Map.Entry<EquipmentSlotKey<S>, MarkerRectangle> entry : slotKeyMap.entrySet()) {
            slotMap.put(entry.getKey(), null);
            markerMap.put(entry.getKey(), entry.getValue());
        }
        this.slotConfigMap.putAll(slotConfigMap);

        invalidateComponents();
    }

    protected abstract void generateSlotKeyMap(Map<String, List<MarkerRectangle>> typeToMarkerMap, Map<EquipmentSlotKey<S>, MarkerRectangle> slotKeyMap, Map<EquipmentSlotKey<S>, SlotConfig> slotNameMap);

    private void resetGraphics() {
        clearComponents();
        slotMap.clear();
        markerMap.clear();
        add(image).grow();
    }

    public void forEachItem(Callable1Args<T> callable) {
        slotMap.values().forEach((item) -> {
            if (item != null) {
                callable.call(item);
            }
        });
    }

    @Override
    public void layout() {
        updateComponents();
        updateItems();
        super.layout();
        if(spriteType == null) return;
        final float scale = image.getImageWidth() / spriteType.getWidth();
        for (GCEquipmentSlotComponent<S, T> component : getComponents().values()) {
            final MarkerRectangle marker = component.getMarker();
            final UPVector position = marker.getPosition(0, 0, spriteType.getHeight());
            final float size = Math.min(marker.width, marker.height);
            component.setIconSize(size * scale);
            component.setOrigin(Align.center);
            component.layout();
            component.setPosition(
                position.x * scale + image.getImageX(),
                position.y * scale + image.getImageY(),
                Align.center);
        }
    }

    @Override
    protected GCEquipmentSlotComponent<S, T> createComponent(EquipmentSlotKey<S> key) {
        final GCEquipmentSlotComponent<S, T> component = new GCEquipmentSlotComponent<>(this, key, markerMap.get(key), slotConfigMap.get(key));
        addActor(component);
        return component;
    }

    @Override
    public void clearComponents() {
        super.clearComponents();
        clearChildren();
        if(inspector != null) {
            inspector.close();
        }
    }

    @Override
    protected Collection<EquipmentSlotKey<S>> getSlots() {
        return slotMap.keySet();
    }

    @Override
    protected T getItem(EquipmentSlotKey<S> key) {
        return slotMap.get(key);
    }

    @Override
    protected List<InventoryAction> getActions(EquipmentSlotKey<S> key) {
        return Collections.emptyList();
    }

    @Override
    protected void onSelected(GCEquipmentSlotComponent<S, T> component) {
        if(inspector == null) {
            if(component == null) {
                return;
            } else {
                inspector = createInspector();
            }
        }
        if(component == null) {
            inspector.close();
        } else {
            inspector.setSlotComponent(component);
            inspector.open(true);
        }
    }

    /**
     * Create and return an inspector window
     */
    protected GCEquipmentInspector<S, T> createInspector() {
        final GCEquipmentInspector<S, T> inspector = new GCEquipmentInspector<>(getLayer());
        inspector.setPositionNextToSlot(true);
        return inspector;
    }

    protected GCEquipmentInspector getInspector () {
        return inspector;
    }

    public void setInspector(GCEquipmentInspector<S, T> inspector) {
        this.inspector = inspector;
    }
}
