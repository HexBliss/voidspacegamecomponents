package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.gamecomponents.client.dialogs.GCStackSplitter;
import com.universeprojects.html5engine.client.framework.H5ELayer;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public abstract class GCInventory<T extends GCInventoryItem> extends GCInventoryBase<Integer, T, GCInventoryItemSlotComponent<Integer, T>> {

    public static final int SCROLL_PANE_PADDING = 5;
    private final Map<Integer, T> items = new TreeMap<>();
    private final Map<Integer, T> items_view = Collections.unmodifiableMap(items);
    private final Table inventoryTable;
    private boolean updateColumns = true;
    private GCInventoryInspector<T> inspector;
    private GCStackSplitter stackSplitter;

    public GCInventory(H5ELayer layer, int count) {
        super(layer);
        left().top();
        defaults().left().top();
        for (int i = 0; i < count; i++) {
            items.put(i, null);
        }
        this.inventoryTable = new Table();
        inventoryTable.left().top();
        inventoryTable.defaults().left().top();
        final ScrollPane scrollPane = new ScrollPane(inventoryTable, getEngine().getSkin());
        scrollPane.setFadeScrollBars(false);
        add(scrollPane).grow().pad(SCROLL_PANE_PADDING);
        addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if(event.getTarget() == GCInventory.this || event.getTarget() == inventoryTable || event.getTarget() == scrollPane) {
                    select(null);
                }
            }
        });
    }

    @Override
    public Table left () {
        super.left();

        if (inventoryTable != null) {
            inventoryTable.left();
            inventoryTable.defaults().left();
        }

        return this;
    }

    @Override
    public Table right () {
        super.right();

        if (inventoryTable != null) {
            inventoryTable.right();
            inventoryTable.defaults().right();
        }

        return this;
    }

    @Override
    public Table top () {
        super.top();

        if (inventoryTable != null) {
            inventoryTable.top();
            inventoryTable.defaults().top();
        }

        return this;
    }

    @Override
    public Table bottom () {
        super.bottom();

        if (inventoryTable != null) {
            inventoryTable.bottom();
            inventoryTable.defaults().bottom();
        }

        return this;
    }

    public void forEachItem(Callable1Args<T> callable) {
        items.values().forEach((item) -> {
            if(item != null) {
                callable.call(item);
            }
        });
    }

    public Map<Integer, T> getItems() {
        return items_view;
    }

    public void addItem(Integer slot, T item) {
        items.put(slot, item);
        invalidateItems();
    }

    public void clearItems() {
        items.entrySet().forEach((entry) -> entry.setValue(null));
        invalidateItems();
    }

    public void removeItem(Integer slot) {
        items.put(slot, null);
        invalidateItems();
    }

    @Override
    protected void sizeChanged() {
        updateColumns = true;
        super.sizeChanged();
    }

    @Override
    public void clearComponents() {
        super.clearComponents();
        if(inspector != null) {
            inspector.close();
        }
    }

    @Override
    public void layout() {
        updateComponents();
        updateItems();
        if(updateColumns) {
            inventoryTable.clearChildren();
            final float width = getWidth() - (SCROLL_PANE_PADDING*2+2);
            float curWidth = 0;
            for(GCInventoryItemSlotComponent<Integer, T> component : getComponents().values()) {
                final float componentWidth = component.getPrefWidth();
                curWidth += componentWidth;
                if(curWidth >= width) {
                    curWidth = componentWidth;
                    inventoryTable.row();
                }
                inventoryTable.add(component).pad(getPadding());
            }
            updateColumns = false;
        }
        super.layout();
    }

    private float getPadding() {
        return 0;
    }

    @Override
    protected GCInventoryItemSlotComponent<Integer, T> createComponent(Integer key) {
        return new GCInventoryItemSlotComponent<>(this, key, getSlotConfig(key));
    }

    protected abstract SlotConfig getSlotConfig(Integer key);

    @Override
    protected Collection<Integer> getSlots() {
        return items.keySet();
    }

    @Override
    protected T getItem(Integer key) {
        return items.get(key);
    }

    @Override
    protected void onSelected(GCInventoryItemSlotComponent<Integer, T> component) {
        if(inspector == null) {
            if(component == null) {
                return;
            } else {
                inspector = createInspector();
            }
        }
        if(component == null) {
            inspector.close();
        } else {
            inspector.setSlotComponent(component);
            inspector.open(true);
        }
    }

    protected void onSplitStack(Integer key, T item) {
        if(stackSplitter != null) {
            stackSplitter.close();
        }
        stackSplitter = new GCStackSplitter(getLayer(), item.getIconSpriteKey(), item.getName(), (int) item.getQuantity()) {
            @Override
            public void onSubmit(int amountToSplit) {
                onStackSplit(key, item, amountToSplit);
            }

            @Override
            public void onClose() {
                this.destroy();
                stackSplitter = null;
            }
        };
        stackSplitter.open();
    }

    protected abstract void onStackSplit(Integer key, T item, int amountToSplit);

    protected abstract void onMergeAll(T item);

    /**
     * Create and return an inspector window
     * @return
     */
    protected GCInventoryInspector<T> createInspector() {
        return new GCInventoryInspector<>(getLayer());
    }

    public abstract void update();

    protected GCInventoryInspector getInspector () {
        return inspector;
    }

    public void setInspector(GCInventoryInspector<T> inspector) {
        this.inspector = inspector;
    }
}
