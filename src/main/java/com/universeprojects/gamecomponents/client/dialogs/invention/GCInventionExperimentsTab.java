package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData.GCInventoryDataItem;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.UPUtils;

class GCInventionExperimentsTab extends GCInventionTab {

    private final H5EScrollablePane listContainer;
    private GCList<GCExperimentsListItem> itemsList;
    private final GCListItemActionHandler<GCExperimentsListItem> listHandler;
    private final H5EInputBox txtRepeats;
    public static final int LIST_SPACING = 5;

    GCInventionExperimentsTab(final GCInventionSystemDialog dialog) {
        super(dialog, "Experimentation");

        listContainer = rightContent.add(new H5EScrollablePane(layer)).left().top().grow().getActor();
        listContainer.getContent().top().left();

        listHandler = new GCListItemActionHandler<GCExperimentsListItem>() {
            @Override
            protected void onSelectionUpdate(GCExperimentsListItem lastSelectedItem) {
                //Reset the repeats text box
                GCExperimentsListItem selectedItem = this.getSelectedItem();
                if (selectedItem != null && !selectedItem.equals(lastSelectedItem)) {
                    resetRepeats();
                }
            }

            @Override
            public void onInfoButtonClicked(GCInventoryDataItem item) {
                dialog.onIconInfoClicked(item.uid);
            }
        };

        //Two columns are needed to get the correct spacing of the label and input box
        leftContent.add().top().left().grow().colspan(2);
        leftContent.row().padBottom(5);

        final H5ELabel txtRepeatsLabel = leftContent.add(new H5ELabel(layer)).fill().getActor();
        txtRepeatsLabel.setText("Repeats: ");

        txtRepeats = leftContent.add(new H5EInputBox(layer)).fillY().growX().getActor();
        txtRepeats.setTypeNumber();
        txtRepeats.setAlignment(Align.right);
        txtRepeats.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                txtRepeats.selectAll();
            }
        });
        resetRepeats();

        final H5EButton btnBeginExperiments = ButtonBuilder.inLayer(layer).withStyle("button2").withText("Begin Experiments").build();
        lowerButtonRow.add(btnBeginExperiments).grow();
        btnBeginExperiments.addButtonListener(() -> {
            int repeats = 1;
            try {
                repeats = Integer.parseInt(txtRepeats.getText());
            } catch (NumberFormatException e) {
                //Ignore
            }

            GCExperimentsListItem selectedItem = listHandler.getSelectedItem();
            Integer selectedItemIdx = selectedItem != null ? selectedItem.data.index : null;
            dialog.onBeginExperimentsBtnPressed(selectedItemIdx, repeats);
        });

        UPUtils.tieButtonToKey(txtRepeats, Input.Keys.ENTER, btnBeginExperiments);
    }

    /**
     * Reset the repeats input box
     */
    public void resetRepeats () {
        txtRepeats.setText("1");
        txtRepeats.selectAll();
        txtRepeats.focus();
    }

    @Override
    public void show () {
        super.show();

        txtRepeats.selectAll();
        txtRepeats.focus();
    }

    @Override
    protected void clearTab() {
        if (itemsList != null) {
            listHandler.reset();
            itemsList.clear();
            itemsList.remove();
            itemsList = null;
            listContainer.getContent().clear();
        }
    }

    void populateData(GCInventoryData inventoryData) {
        clearTab();

        itemsList = new GCList<>(layer, 1, LIST_SPACING, LIST_SPACING, true);
        listContainer.add(itemsList).left().top().grow();
        for (int index = 0; index < inventoryData.size(); index++) {
            GCInventoryDataItem dataItem = inventoryData.get(index);
            GCExperimentsListItem listItem = new GCExperimentsListItem(layer, listHandler, dataItem);
            itemsList.addItem(listItem);
        }
    }
}
