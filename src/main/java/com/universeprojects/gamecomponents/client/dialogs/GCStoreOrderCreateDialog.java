package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.List;

public class GCStoreOrderCreateDialog extends GCSimpleWindow {
    private final H5EInputBox offerItemNameBox;
    private final H5EInputBox offerItemQuantityBox;

    private final H5EInputBox paymentItemNameBox;
    private final H5EInputBox paymentItemQuantityBox;

    // Parallel lists for the item name and quantity for offer
    List<String> offerItemNames;
    List<Integer> offerItemQuantities;

    // Parallel lists for the item name and quantity for payment
    List<String> paymentItemNames;
    List<Integer> paymentItemQuantities;

    public GCStoreOrderCreateDialog(H5ELayer layer) {
        super(layer, "storeOrderCreateWindow", "Create Store Order");
        this.positionProportionally(0.5f,0.5f);

        H5ELabel offerItemNameLabel = new H5ELabel("Offer Item Name", layer);
        this.add(offerItemNameLabel);
        H5ELabel offerItemQuantityLabel = new H5ELabel("Quantity", layer);
        this.add(offerItemQuantityLabel).right();

        this.row();

        Cell<H5EInputBox> offerItemNameBoxCell = this.addInputBox(300);
        Cell<H5EInputBox> offerItemQuantityBoxCell = this.addInputBox(100);
        offerItemNameBox = offerItemNameBoxCell.getActor();
        offerItemQuantityBox = offerItemQuantityBoxCell.getActor();

        this.row();

        H5ELabel paymentItemNameLabel = new H5ELabel("Payment Item Name", layer);
        this.add(paymentItemNameLabel);
        H5ELabel paymentItemQuantityLabel = new H5ELabel("Quantity", layer);
        this.add(paymentItemQuantityLabel).right();

        this.row();

        Cell<H5EInputBox> paymentItemNameBoxCell = this.addInputBox(300);
        Cell<H5EInputBox> paymentItemQuantityBoxCell = this.addInputBox(100);
        paymentItemNameBox = paymentItemNameBoxCell.getActor();
        paymentItemQuantityBox = paymentItemQuantityBoxCell.getActor();

        this.row();

        H5EButton createButton = ButtonBuilder.inLayer(layer)
                .withStyle("button-small")
                .withText("Create")
                .withFontScale(0.9f)
                .build();

        createButton.addButtonListener(new Callable0Args() {
            @Override
            public void call() {
                createStoreOrder();
            }
        });

        this.add(createButton);
    }

    public String getItemNameForOffer() {
        return offerItemNameBox.getText();
    }

    public String getItemQuantityForOfferAsString() {
        return offerItemQuantityBox.getText();
    }

    public String getItemNameForPayment() {
        return paymentItemNameBox.getText();
    }

    public String getItemQuantityForPaymentAsString() {
        return paymentItemQuantityBox.getText();
    }

    /**
     * Method to override on construction
     */
    public void createStoreOrder () {

    }
}
