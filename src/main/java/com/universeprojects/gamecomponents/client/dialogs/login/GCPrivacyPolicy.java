package com.universeprojects.gamecomponents.client.dialogs.login;


import com.universeprojects.html5engine.client.framework.H5ELayer;


public class GCPrivacyPolicy extends GCPolicyDialog {

    private final GCLoginManager<?, ?, ?> loginManager;

    public GCPrivacyPolicy(H5ELayer layer, GCLoginManager<?, ?, ?> loginManager){
        super(layer);
        this.loginManager = loginManager;
        window.setTitle("Privacy Policy");
        btnAccept.addButtonListener(this::accept);
        btnBack.addButtonListener(this::back);
    }

    private void back() {
        close();
        loginManager.openLoginScreen();
    }

    private void accept() {
        close();
        loginManager.openRegisterWindow();
    }

    @Override
    public void open() {
        super.open();
        loadingIndicator.activate(window);
        loginManager.loadPrivacyPolicy((text) -> {
            loadingIndicator.deactivate();
            setText(text);
        }, (error) -> {
            loadingIndicator.deactivate();
            //TODO
        });
    }

}
