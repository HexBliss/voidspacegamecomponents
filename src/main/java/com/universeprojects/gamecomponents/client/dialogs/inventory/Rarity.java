package com.universeprojects.gamecomponents.client.dialogs.inventory;

public enum Rarity {
    JUNK,
    COMMON,
    UNCOMMON,
    RARE,
    EPIC,
    LEGENDARY;

    public String getIconStyle() {
        return "icon-" + name().toLowerCase() + "_grade";
    }
}
