package com.universeprojects.gamecomponents.client.dialogs.login;


import com.universeprojects.html5engine.client.framework.H5ELayer;


public class GCTermsOfService extends GCPolicyDialog {

    private final GCLoginManager<?, ?, ?> loginManager;

    public GCTermsOfService(H5ELayer layer, GCLoginManager<?, ?, ?> loginManager) {
        super(layer);
        this.loginManager = loginManager;
        window.setTitle("Terms Of Service");
        btnAccept.addButtonListener(this::accept);
        btnBack.addButtonListener(this::back);
    }

    private void back() {
        close();
        loginManager.openLoginScreen();
    }

    private void accept() {
        close();
        loginManager.openPrivacyPolicyWindow();
    }

    @Override
    public void open() {
        super.open();
        loadingIndicator.activate(window);
        loginManager.loadTermsOfService((text) -> {
            loadingIndicator.deactivate();
            setText(text);
        }, (error) -> {
            loadingIndicator.deactivate();
            //TODO
        });
    }
}
