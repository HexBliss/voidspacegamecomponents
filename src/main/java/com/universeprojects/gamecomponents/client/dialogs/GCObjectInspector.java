package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5EContainer;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;


public class GCObjectInspector {

    protected final GCWindow window;

    private final H5ELabel parameterKeys;
    private final H5ELabel parametersValues;

    private final Map<String, String> parameters = new LinkedHashMap<>();
    public static final int ICON_SIZE = 64;

    public GCObjectInspector(H5ELayer layer, String iconSpriteTypeKey, String objectName,
                             String objectType, String description, Map<String, String> parameters) {

        window = new GCWindow(layer, 300, 300, "inspector-window");
        window.setId("object-inspector");
        window.setTitle("INSPECTOR");
        window.positionProportionally(0.5f, 0.5f);
        window.setPackOnOpen(true);
        window.getTitleTable().removeActor(window.getTitleLabel());
        window.defaults().left();

        // space occupied even if there is no icon
        H5EContainer iconPlaceholder = new H5EContainer(layer);
        iconPlaceholder.setWidth(ICON_SIZE);
        iconPlaceholder.setHeight(ICON_SIZE);
        Cell<H5EContainer> iconPlaceholderCell = window.add(iconPlaceholder);
        iconPlaceholderCell.prefWidth(ICON_SIZE).prefHeight(ICON_SIZE).center();

        final float titleFontSize = 1f;
        final float contentFontSize = 0.8f;

        if (iconSpriteTypeKey != null) {
            // place the icon on top of the invisible placeholder
            H5EIcon icon = H5EIcon.fromSpriteType(layer, iconSpriteTypeKey);
            iconPlaceholder.add(icon).width(ICON_SIZE).height(ICON_SIZE);
        }

        Table nameTable = new Table();
        window.add(nameTable).colspan(3).left();
        H5ELabel objectNameLabel = new H5ELabel(layer);
        objectNameLabel.setFontScale(titleFontSize);
        objectNameLabel.setText(objectName);
        objectNameLabel.setAlignment(Align.left);
        final Cell<H5ELabel> objectNameLabelCell = nameTable.add(objectNameLabel);
        objectNameLabelCell.padTop(10).left();

        nameTable.row();
        H5ELabel objectTypeLabel = new H5ELabel(layer);
        objectTypeLabel.setFontScale(contentFontSize);
        objectTypeLabel.setText(objectType);
        objectTypeLabel.setAlignment(Align.left);
        final Cell<H5ELabel> objectTypeLabelCell = nameTable.add(objectTypeLabel);
        objectTypeLabelCell.padTop(2).padLeft(25).left();

        window.row().padTop(20);

        H5ELabel descriptionTextArea = new H5ELabel(layer);
        descriptionTextArea.setWrap(true);
        descriptionTextArea.setFontScale(contentFontSize);
        descriptionTextArea.setText(description);
        final Cell<H5ELabel> descriptionTextAreaCell = window.add(descriptionTextArea);
        descriptionTextAreaCell.colspan(4).fillX().width(400);

        window.row().padTop(20);

        parameterKeys = new H5ELabel(layer);
        parameterKeys.setWrap(true);
        parameterKeys.setFontScale(contentFontSize);
        window.add(parameterKeys).colspan(2).padBottom(10);

        parametersValues = new H5ELabel(layer);
        parametersValues.setWrap(true);
        parametersValues.setFontScale(contentFontSize);
        window.add(parametersValues).colspan(2).padBottom(10);

        setParameters(parameters);
    }

    private void updateParameters() {
        StringBuilder keys = new StringBuilder();
        StringBuilder values = new StringBuilder();

//        double keyW = 0;
//        double valueW = 0;

        boolean first = true;
        for (String key : parameters.keySet()) {
            String value = parameters.get(key);
            key = " \u25CF  " + key + ":";

            if (first) {
                first = false;
            } else {
                keys.append("\n");
                values.append("\n");
            }

            keys.append(key);
            values.append(value);

//            keyW = UPMath.max(keyW, ctx.measureText(key).getWidth());
//            valueW = UPMath.max(valueW, ctx.measureText(value).getWidth());
        }

        parameterKeys.setText(keys.toString());
//        parameterKeys.setWidth((int) UPMath.ceil(keyW) + 4);

        parametersValues.setText(values.toString());
//        parametersValues.setWidth((int) UPMath.ceil(valueW) + 4);

//        window.updateElementOffset(parametersValues, parameterKeys.getWidth() + 5, 0);
    }

    public void setParameters(Map<String, String> newParams) {
        parameters.clear();
        if (newParams != null) {
            parameters.putAll(newParams);
        }
        updateParameters();
    }

    public Map<String, String> getParameters() {
        return Collections.unmodifiableMap(parameters);
    }

    public void setParameter(String key, String value) {
        parameters.put(key, value);
        updateParameters();
    }

    public void open() {
        window.open();
    }

    public void close() {
        window.close();
    }

    public boolean isOpen() {
        return window.isOpen();
    }

    public void positionProportionally(float propX, float propY) {
        window.positionProportionally(propX, propY);
    }
}
