package com.universeprojects.gamecomponents.client.dialogs.login;


import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.elements.GCLabel;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

/**
 * Creates the UI for players to login.
 */
public class GCCharacter<C extends CharacterInfo> extends GCListItem {

    private final String characterName;
    private H5ELabel nameLabel;

    private final C characterInfo;

    public C getCharacterInfo() {
        return characterInfo;
    }

    public GCCharacter(H5ELayer layer, GCListItemActionHandler<GCCharacter<C>> actionHandler, C characterInfo) {
        super(layer, actionHandler);

        characterName = characterInfo.getName();
        nameLabel = add(new GCLabel(layer)).left().growX().padLeft(5).height(50).getActor();
        this.characterInfo = characterInfo;
        nameLabel.setText(characterName);
        nameLabel.setAlignment(Align.center);


    }
}





