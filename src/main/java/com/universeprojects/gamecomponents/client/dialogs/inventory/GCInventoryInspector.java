package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class GCInventoryInspector<T extends GCInventoryItem> extends GCItemInspector<Integer, T> {

    public static final int BUTTON_SIZE = 40;
    private final ImageTextButton mergeAllButton;
    private final ImageTextButton splitStackButton;

    public GCInventoryInspector(H5ELayer layer) {
        super(layer);
        Table table = add(new Table()).colspan(2).fill().getActor();

        mergeAllButton = table.add(new ImageTextButton("", getEngine().getSkin(), "btn-merge-all")).size(BUTTON_SIZE).grow().getActor();
        mergeAllButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onMergeAllButtonClicked();
                event.handle();
            }
        });
        splitStackButton = table.add(new ImageTextButton("", getEngine().getSkin(), "btn-split-stack")).size(BUTTON_SIZE).grow().getActor();
        splitStackButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onSplitStackButtonClicked();
                event.handle();
            }
        });
    }

    private void onMergeAllButtonClicked() {
        if(currentComponent == null || currentComponent.getItem() == null) return;
        //noinspection unchecked
        ((GCInventory)currentComponent.inventoryBase).onMergeAll(currentComponent.getItem());
    }

    private void onSplitStackButtonClicked() {
        if(currentComponent == null || currentComponent.getItem() == null) return;
        //noinspection unchecked
        ((GCInventory)currentComponent.inventoryBase).onSplitStack(currentComponent.getKey(), currentComponent.getItem());
    }

    @Override
    protected void onSetSlotComponent(T item, GCInventorySlotComponent<Integer, T, ?> component) {
        mergeAllButton.setVisible(item.isStackable());
        splitStackButton.setVisible(item.isStackable() && item.getQuantity() > 1);
    }
}
